import 'package:objectbox/objectbox.dart';

//command to generate file | flutter pub run build_runner build --delete-conflicting-outputs

@Entity()
class UserEntity {
  int id = 0;

  @Index(type: IndexType.value)
  @Unique()
  String? userUID;

  DateTime? userCreationTime;
  DateTime? userLastSignInTime;
  String? userAddress;
  int? userCart;
  String? userCity;
  int? userCompletedOrders;
  String? userEmail;
  bool? userEmailVerified;
  String? userGender;
  String? userName;
  int? userPendingOrders;
  String? userPhone;
  bool? userPhoneVerified;
  String? userState;
  int? userWish;
  String? firebaseMessagingToken;

  String? razorPayCustomerID;
  int? razorPayCreatedAt;

  UserEntity({
    this.userUID,
    this.userAddress,
    this.userCart,
    this.userCity,
    this.userCompletedOrders,
    this.userWish,
    this.userState,
    this.userPhoneVerified,
    this.userPendingOrders,
    this.userPhone,
    this.userLastSignInTime,
    this.userName,
    this.userGender,
    this.userEmailVerified,
    this.userCreationTime,
    this.userEmail,
    this.firebaseMessagingToken,
    this.razorPayCustomerID,
    this.razorPayCreatedAt,
  });
}
