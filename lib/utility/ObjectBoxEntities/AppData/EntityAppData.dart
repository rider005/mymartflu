import 'package:objectbox/objectbox.dart';

//command to generate file | flutter pub run build_runner build --delete-conflicting-outputs

@Entity()
class EntityAppData {
  int id = 0;

  @Index(type: IndexType.value)
  String userCurrentLanguage;

  EntityAppData({
    required this.userCurrentLanguage,
  });
}
