import 'package:objectbox/objectbox.dart';

//command to generate file | flutter pub run build_runner build

@Entity()
class ProductCategoriesEntity {
  int id = 0;

  @Index(type: IndexType.value)
  @Unique()
  String? catID;

  String? catImg;
  String? catGujaratiName;
  String? catHindiName;
  String? catName;
  bool? isActive;
  final products = ToMany<ProductsListEntity>();

  ProductCategoriesEntity(
      {this.catID,
      this.catImg,
      this.catGujaratiName,
      this.catName,
      this.isActive,
      this.catHindiName});
}

@Entity()
class ProductsListEntity {
  int id = 0;

  final productCat = ToOne<ProductCategoriesEntity>();

  final productDetails = ToOne<ProductDetailsEntity>();
  @Index(type: IndexType.value)
  @Unique()
  String? productID;

  String? productImg;
  String? productName;

  ProductsListEntity({
    this.productID,
    this.productImg,
    this.productName,
  });
}

@Entity()
class ProductDetailsEntity {
  int id = 0;

  final product = ToOne<ProductsListEntity>();

  int? qtyInCart = 0;

  bool? isInWish = false;
  bool? isInCart = false;

  String? productPrice;
  String? productCompanyName;
  String? productWeight;
  String? addedInCartTime;

  ProductDetailsEntity(
      {this.isInWish,
      this.isInCart,
      this.productPrice,
      this.productCompanyName,
      this.productWeight,
      this.addedInCartTime,
      this.qtyInCart});
}

@Entity()
class OrderEntity {
  int id = 0;

  // Add Order ID field and use it for unique order for .where condition
  @Unique()
  @Index(type: IndexType.value)
  String? orderID;

  String? razorPayOrderID;

  String? orderedTime;
  String? orderedToAddress;
  String? sellerPersonID;
  double? orderedTotalPrice;

  final items = ToMany<OrderItemEntity>();

  OrderEntity({
    this.orderID,
    this.razorPayOrderID,
    this.sellerPersonID,
    this.orderedTime,
    this.orderedToAddress,
    this.orderedTotalPrice,
  });
}

@Entity()
class OrderItemEntity {
  int id = 0;

  String? catID;
  String? productID;
  String? productPrice;
  int? qtyForOrder;
}
