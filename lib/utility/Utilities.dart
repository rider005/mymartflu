import 'dart:convert';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:my_mart/services/FireStoreServices/UserServices.dart';
import 'package:path_provider/path_provider.dart';

import '../main.dart';
import '../objectbox.g.dart';
import 'ObjectBoxEntities/AppData/EntityAppData.dart';
import 'ObjectBoxEntities/ProductCat/EntityForProducts.dart';
import 'ObjectBoxEntities/User/UserEntity.dart';

// This file has use-full methods which we need to for multiple files

class Utilities {
  String username =
      kDebugMode ? 'rzp_test_9eM4YXptyjjvQS' : 'rzp_live_CaJKG0QqjHJjah';
  String password =
      kDebugMode ? 'ofWaPzgWZyo9q6oBbosiQXnj' : 'cacxVjEDhHYjtf6P68kYDftp';

  String razorPayApiUrl = 'https://api.razorpay.com/v1/';

  getRazorPayOrderUrl() {
    return '${razorPayApiUrl}orders';
  }

  Future razorPayRequestByURLAndBody(bool isTest, String url, body) async {
    // String username =
    //     isTest ? 'rzp_test_9eM4YXptyjjvQS' : 'rzp_live_CaJKG0QqjHJjah';
    // String password =
    //     isTest ? 'ofWaPzgWZyo9q6oBbosiQXnj' : 'cacxVjEDhHYjtf6P68kYDftp';
    String basicAuth =
        'Basic ${base64.encode(utf8.encode('$username:$password'))}';
    return await http.post(Uri.parse(url),
        headers: <String, String>{
          'Content-Type': 'application/json',
          'authorization': basicAuth
        },
        body: body);
  }

  validateUserName(String userName) {
    if (userName.isEmpty) {
      return 'User Name is required';
    } else if (!RegExp(r'^[A-Za-z0-9]+(?:[ _-][A-Za-z0-9]+)*$')
        .hasMatch(userName)) {
      return 'Please enter valid user name';
    } else {
      return null;
    }
  }

  validateEmail(String userEmail) {
    if (userEmail.isEmpty) {
      return 'Email is required';
    } else if (!RegExp(
            r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
        .hasMatch(userEmail)) {
      return 'Please enter valid email';
    } else {
      return null;
    }
  }

  validateName(String userName) {
    if (userName.isEmpty) {
      return 'Name is required';
    } else if (userName.length < 3) {
      return 'Name should have minimum 3 letters';
    } else {
      return null;
    }
  }

  validatePhone(String userPhone) {
    if (userPhone.isEmpty) {
      return 'Phone number is required';
    } else if (userPhone.length < 10 || userPhone.length > 10) {
      return 'Number should have only 10 digits';
    } else {
      return null;
    }
  }

  validateAddress(String userAddress) {
    if (userAddress.isEmpty) {
      return 'Address is required';
    } else if (userAddress.length < 10) {
      return 'Write valid and full address';
    } else {
      return null;
    }
  }

  validatePass(String userPass) {
    if (userPass.isEmpty) {
      return 'Password is required';
    } else if (!RegExp(
            r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#\$&*~]).{8,}$')
        .hasMatch(userPass)) {
      return 'Please enter valid password';
    } else {
      return null;
    }
  }

  showSnackBar(String message) {
    ScaffoldMessenger.of(navigatorKey.currentContext!)
        .showSnackBar(SnackBar(content: Text(message)));
  }

  startScreenTracking(String screenName) async {
    try {
      await firebaseAnalytics.setCurrentScreen(screenName: screenName);
    } catch (e) {
      if (kDebugMode) {
        print(e);
      }
    }
  }

  Future<bool> get userVerified async {
    await auth.currentUser?.reload();
    return auth.currentUser?.emailVerified ?? false;
  }

  Future createRazorPayCustomer(User user) async {
    var reqBody = jsonEncode(<String, dynamic>{
      'name': user.displayName ?? '',
      'contact': user.phoneNumber ?? '',
      'email': user.email.toString(),
      'fail_existing': '0',
      'notes': {
        'emailVerified': user.emailVerified,
        'firebaseAuthUserUID': user.uid,
      }
    });
    var resBody = await razorPayRequestByURLAndBody(
        kDebugMode ? true : false, '${razorPayApiUrl}customers', reqBody);
    // debugPrint('B ID : ${body['id']??''}');
    var body = jsonDecode(resBody.body);
    // if (body['error']['description'].toString().length>1) {
    //   debugPrint('Body : ${body['error']['description']}');
    //   return;
    // }
    // debugPrint('Body : $body');
    // debugPrint('Body ID : ${body['id']}');
    Box userBox = objectboxStore.box<UserEntity>();
    var localUser = userBox.getAll();
    if (localUser.isNotEmpty) {
      localUser.first.razorPayCustomerID = body['id'];
      localUser.first.razorPayCreatedAt = body['created_at'];
      userBox.put(localUser.first);
    }
  }

  listenUserAuthAndFirebaseMessagingChanges() async {
    auth.userChanges().listen((User? user) async {
      if (user == null) {
        debugPrint('User is currently signed out!');
        return;
      }
      debugPrint('User is signed in!');
      await createRazorPayCustomer(user);
      if (!user.emailVerified) {
        try {
          bool userEmailVerified = await userVerified;
          if (userEmailVerified) {
            await UserServices()
                .getCurrentUserRef(user.uid)
                .update({'userEmailVerified': userEmailVerified});
            debugPrint('User Email verified set to true!');
          }
        } catch (e) {
          debugPrint(
              'listenUserAuthChanges : Catch block : emailVerified setting');
        }
      }
      firebaseMessaging.onTokenRefresh.listen((fcmToken) async {
        // Note: This callback is fired at each app startup and whenever a new
        // token is generated.
        try {
          await UserServices()
              .getCurrentUserRef(user.uid)
              .update({'firebaseMessagingToken': fcmToken});
          debugPrint('fcmToken : $fcmToken');
        } catch (e) {
          debugPrint('ERROR firebaseMessagingToken Updating');
        }
      }).onError((err) {
        // Error getting token.
        debugPrint('ERROR FirebaseMessaging token Generation');
      });
      FirebaseMessaging.onMessage.listen((RemoteMessage message) async {
        debugPrint('Got a message whilst in the foreground!');
        debugPrint('Message data: ${message.data}');
        try {
          await showNotificationFromFirebase(message);
        } catch (e) {
          debugPrint('CATCH : showNotificationFromFirebase');
        }
      });
    });
  }

  String getCurrentLan() {
    var currentLan = 'gu';
    switch (Get.locale.toString().substring(0, 2)) {
      case 'gu':
        currentLan = 'gu';
        break;
      case 'hi':
        currentLan = 'hi';
        break;
      case 'en':
        currentLan = 'en';
        break;
      default:
        currentLan = 'en';
        break;
    }
    return currentLan;
  }

  translateLongNumber(String mobileNumber) {
    var mobileNumberTrim = mobileNumber.toString().trim();
    var translatedNumber = '';
    var currentLan = getCurrentLan();
    if (currentLan == 'en') {
      return mobileNumberTrim;
    }
    for (int i = 0; i < mobileNumberTrim.length; i++) {
      translatedNumber =
          translatedNumber + translateNumber(mobileNumberTrim[i]);
    }
    return translatedNumber;
  }

  translateNumber(String number) {
    var currentLan = getCurrentLan();
    switch (number) {
      case '0':
        return currentLan == 'gu'
            ? '૦'
            : currentLan == 'en'
                ? number
                : '૦';
      case '1':
        return currentLan == 'gu'
            ? '૧'
            : currentLan == 'en'
                ? number
                : '१';
      case '2':
        return currentLan == 'gu'
            ? '૨'
            : currentLan == 'en'
                ? number
                : '२';
      case '3':
        return currentLan == 'gu'
            ? '૩'
            : currentLan == 'en'
                ? number
                : '३';
      case '4':
        return currentLan == 'gu'
            ? '૪'
            : currentLan == 'en'
                ? number
                : '४';
      case '5':
        return currentLan == 'gu'
            ? '૫'
            : currentLan == 'en'
                ? number
                : '५';
      case '6':
        return currentLan == 'gu'
            ? '૬'
            : currentLan == 'en'
                ? number
                : '६';
      case '7':
        return currentLan == 'gu'
            ? '૭'
            : currentLan == 'en'
                ? number
                : '७';
      case '8':
        return currentLan == 'gu'
            ? '૮'
            : currentLan == 'en'
                ? number
                : '८';
      case '9':
        return currentLan == 'gu'
            ? '૯'
            : currentLan == 'en'
                ? number
                : '९';
      default:
        return number;
    }
  }

  deleteWholeDataFromLocal() async {
    try {
      objectboxStore.box<ProductCategoriesEntity>().removeAll();
      objectboxStore.box<ProductsListEntity>().removeAll();
      objectboxStore.box<ProductDetailsEntity>().removeAll();
      objectboxStore.box<OrderEntity>().removeAll();
      objectboxStore.box<OrderItemEntity>().removeAll();
      objectboxStore.box<UserEntity>().removeAll();
      objectboxStore.box<EntityAppData>().removeAll();
      bottomNavSelectedIndex.value = 0;
    } catch (e) {
      debugPrint('deleteWholeDataFromLocal E : $e');
      await _deleteCacheDir();
      await _deleteAppDir();
    }
  }

  Future<void> _deleteCacheDir() async {
    final cacheDir = await getTemporaryDirectory();

    if (cacheDir.existsSync()) {
      cacheDir.delete(recursive: true);
    }
  }

  Future<void> _deleteAppDir() async {
    final appDir = await getApplicationSupportDirectory();

    if (appDir.existsSync()) {
      appDir.delete(recursive: true);
    }
  }

  getMonthName(int month) {
    var currentLan = getCurrentLan();
    switch (month) {
      case 1:
        return currentLan == 'gu'
            ? 'જાન્યુઆરી'
            : currentLan == 'hi'
                ? 'जनवरी'
                : 'January';
      case 2:
        return currentLan == 'gu'
            ? 'ફેબ્રુઆરી'
            : currentLan == 'hi'
                ? 'फरवरी'
                : 'February';
      case 3:
        return currentLan == 'gu'
            ? 'માર્ચ'
            : currentLan == 'hi'
                ? 'मार्च'
                : 'March';
      case 4:
        return currentLan == 'gu'
            ? 'એપ્રિલ'
            : currentLan == 'hi'
                ? 'अप्रैल'
                : 'April';
      case 5:
        return currentLan == 'gu'
            ? 'મે'
            : currentLan == 'hi'
                ? 'मई'
                : 'May';
      case 6:
        return currentLan == 'gu'
            ? 'જૂન'
            : currentLan == 'hi'
                ? 'जून'
                : 'Jun';
      case 7:
        return currentLan == 'gu'
            ? 'જુલાઈ'
            : currentLan == 'hi'
                ? 'जुलाई'
                : 'July';
      case 8:
        return currentLan == 'gu'
            ? 'ઑગસ્ટ'
            : currentLan == 'hi'
                ? 'अगस्त'
                : 'August';
      case 9:
        return currentLan == 'gu'
            ? 'સપ્ટેમ્બર'
            : currentLan == 'hi'
                ? 'सितम्बर'
                : 'September';
      case 10:
        return currentLan == 'gu'
            ? 'ઓક્ટોબર'
            : currentLan == 'hi'
                ? 'अक्टूबर'
                : 'October';
      case 11:
        return currentLan == 'gu'
            ? 'નવેમ્બર'
            : currentLan == 'hi'
                ? 'नवम्बर'
                : 'November';
      case 12:
        return currentLan == 'gu'
            ? 'ડિસેમ્બર'
            : currentLan == 'hi'
                ? 'दिसंबर'
                : 'December';
      default:
        return 'not valid month';
    }
  }

  Widget clipImg(Widget child) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(25),
      child: child,
    );
  }

  Widget customContainer(Widget child) {
    // custom
    return Container(
      decoration: BoxDecoration(
        boxShadow: [
          BoxShadow(
              color: Get.isDarkMode ? Colors.grey : Colors.green,
              blurRadius: 25,
              spreadRadius: -17)
        ],
        borderRadius: BorderRadius.circular(25),
      ),
      child: child,
    );
  }

  Widget customCard(bool isSelected, Widget child, {Color? color}) {
    return Card(
      color: Get.isDarkMode ? Colors.black54 : color ?? Colors.white,
      elevation: !Get.isDarkMode ? 7 : 3.0,
      surfaceTintColor: isSelected
          ? Get.isDarkMode
              ? Colors.black
              : Colors.green[900]
          : Colors.white,
      shadowColor: Get.isDarkMode ? Colors.white : Colors.green,
      child: child,
    );
  }

// getCustomizedNestedScrollView(String title, Widget child) {
//   return NestedScrollView(
//     headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
//       SliverAppBar(
//         floating: true,
//         title: Text(title),
//         actions: [Container()],
//       );
//     },
//     body: child,
//   );
// }

  Future<String> getFirebaseMessagingToken() async {
    return (await firebaseMessaging.getToken(
        vapidKey:
            'BHqLSfx22O3QbcG7WoMrevaS0qoIA_WTCtwhKtZD4BfNeGunuuGCKn2BAwN7vnsRL-cw_1sEgjlW89qoZeYEosk'))!;
  }
}

void onDidReceiveLocalNotification(
    int id, String? title, String? body, String? payload) async {}

Future<void> showNotificationFromFirebase(RemoteMessage message) async {
  // If you're going to use other Firebase services in the background, such as Firestore,
  // make sure you call `initializeApp` before using other Firebase services.
  // await Firebase.initializeApp(
  //   options: DefaultFirebaseOptions.currentPlatform,
  // );
  RemoteNotification? notification = message.notification;
  AndroidNotification? android = message.notification?.android;
  if (notification != null && android != null) {
    debugPrint(
        'Message also contained a notification: ${message.notification}');
    FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
        FlutterLocalNotificationsPlugin();

    const AndroidInitializationSettings initializationSettingsAndroid =
        AndroidInitializationSettings('@mipmap/ic_launcher');

    const DarwinInitializationSettings initializationSettingsIOS =
        DarwinInitializationSettings(
            requestAlertPermission: true,
            requestBadgePermission: true,
            requestSoundPermission: true,
            onDidReceiveLocalNotification: onDidReceiveLocalNotification);

    const InitializationSettings initializationSettings =
        InitializationSettings(
            android: initializationSettingsAndroid,
            iOS: initializationSettingsIOS,
            macOS: initializationSettingsIOS);
    await flutterLocalNotificationsPlugin.initialize(initializationSettings);
    flutterLocalNotificationsPlugin.show(
        notification.hashCode,
        notification.title,
        notification.body,
        NotificationDetails(
          android: AndroidNotificationDetails(
              'high_importance_channel', 'High Importance Notifications',
              channelDescription:
                  'This channel is used for important notifications.',
              icon: android.smallIcon,
              importance: Importance.max
              // other properties...
              ),
        ));
  }
}

class ObjectBox {
  /// The Store of this app.
  static late Store store;

  ObjectBox._create(store) {
    // Add any additional setup code, e.g. build queries.
  }

  /// Create an instance of ObjectBox to use throughout the app.
  static Future<ObjectBox> create() async {
    // Future<Store> openStore() {...} is defined in the generated objectbox.g.dart
    // final store = await openStore();
    return ObjectBox._create(store);
  }
}

extension StringCasingExtension on String {
  String toCapitalized() =>
      length > 0 ? '${this[0].toUpperCase()}${substring(1).toLowerCase()}' : '';

  String toTitleCase() => replaceAll(RegExp(' +'), ' ')
      .split(' ')
      .map((str) => str.toCapitalized())
      .join(' ');
}
