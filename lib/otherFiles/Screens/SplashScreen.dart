import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import 'package:my_mart/BottomNavigationScreen.dart';
import 'package:my_mart/auth/Login.dart';
import 'package:my_mart/services/AuthServices.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  SplashScreenState createState() => SplashScreenState();
}

class SplashScreenState extends State<SplashScreen>
    with SingleTickerProviderStateMixin {
  late AnimationController _controller;

  @override
  void initState() {
    super.initState();
    var isLoggedIn = false;
    _controller = AnimationController(vsync: this);
    try {
      if (AuthServices().currentUser != null) {
        isLoggedIn = true;
        // showNextScreen(isLoggedIn);
      } else {
        // showNextScreen(isLoggedIn);
      }
    } catch (e) {
      print(e);
      showNextScreen(isLoggedIn);
    }
    _controller.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        showNextScreen(isLoggedIn);
        _controller.reset();
      }
    });
  }

  @override
  dispose() {
    _controller.dispose();
    super.dispose();
  }

  showNextScreen(bool isLoggedIn) {
    Navigator.of(context).pushReplacement(MaterialPageRoute(
        builder: (BuildContext context) => !isLoggedIn
            ? const LoginScreen()
            : const BottomNavigationScreen()));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(
        child: Lottie.network(
            'https://assets4.lottiefiles.com/packages/lf20_fdkjwadf.json',
            controller: _controller,
            frameRate: FrameRate.max,
            repeat: false, onLoaded: (composite) {
          _controller.duration = composite.duration;
          _controller.forward();
        }),
      ),
    );
  }
}
