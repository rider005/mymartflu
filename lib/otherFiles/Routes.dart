import 'package:flutter/cupertino.dart';
import 'package:my_mart/auth/EnterOTPScreen.dart';
import 'package:my_mart/auth/Login.dart';
import 'package:my_mart/products/ProductDetails.dart';
import 'package:my_mart/products/ProductsList.dart';

import '../BottomNavigationScreen.dart';
import 'Constants.dart';

var routesList = <String, WidgetBuilder>{
  LOGIN_SCREEN: (BuildContext context) => const LoginScreen(),
  DASHBOARD_SCREEN: (BuildContext context) => const BottomNavigationScreen(),
  PRODUCTS_LIST_SCREEN: (BuildContext context) => const ProductsListScreen(
        catID: '',
        catName: '',
      ),
  PRODUCT_DETAILS_SCREEN: (BuildContext context) => const ProductDetailsScreen(
        catId: '',
        productId: '',
        productInfo: {'productName': '', 'productImg': '', 'catName': ''},
      ),
  ENTER_OTP_SCREEN: (BuildContext context) =>
      const EnterOTPScreen(userPhone: ''),
};
