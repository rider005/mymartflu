import 'package:animations/animations.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:my_mart/utility/ObjectBoxEntities/ProductCat/EntityForProducts.dart';
import 'package:my_mart/utility/Utilities.dart';

import 'main.dart';
import 'objectbox.g.dart';
import 'otherFiles/Screens/ShowLoadingScreen.dart';
import 'products/ProductDetails.dart';

class DashboardScreen extends StatefulWidget {
  const DashboardScreen({Key? key}) : super(key: key);

  @override
  State<DashboardScreen> createState() => _DashboardScreenState();
}

class _DashboardScreenState extends State<DashboardScreen> {
  RxList<ProductsListEntity> productsList = <ProductsListEntity>[].obs;
  TextEditingController controller = TextEditingController();
  late Stream<List<ProductsListEntity>> _stream;
  Box<ProductsListEntity> box = objectboxStore.box<ProductsListEntity>();
  RxBool showSearchClearIcon = false.obs;
  final focusNode = FocusNode();
  late List<ProductsListEntity> productsListFromDB;

  @override
  void initState() {
    super.initState();
    // 👇 ADD THIS
    _stream = objectboxStore
        .box<ProductsListEntity>()
        // The simplest possible query that just gets ALL the data out of the Box
        .query()
        .watch(triggerImmediately: true)
        // Watching the query produces a Stream<Query<ProductCategories>>
        // To get the actual data inside a List<ProductCategories>, we need to call find() on the query
        .map((query) => query.find());
    productsListFromDB = box.getAll();
  }

  onSearchTextChanged(String text) async {
    showSearchClearIcon.value = text.isEmpty ? false : true;
    controller.text = text;
    controller.selection = TextSelection.collapsed(offset: text.length);
    if (text.isEmpty) {
      productsList.value = productsListFromDB;
      return;
    }
    productsList.value = productsListFromDB
        .where((element) =>
            element.productName!.toLowerCase().contains(text.toLowerCase()))
        .toList();
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
        stream: _stream,
        builder: (ctx, snapshot) {
          if (snapshot.hasData && !snapshot.hasError) {
            productsList.value = (snapshot.data as List<ProductsListEntity>);
            if (productsList.isEmpty) {
              return const Center(child: Text('Explore products to see here!'));
            }
            return Obx(() => Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Card(
                        child: ListTile(
                          leading: const Icon(Icons.search),
                          title: TextField(
                            focusNode: focusNode,
                            controller: controller,
                            decoration: const InputDecoration(
                                hintText: 'Search', border: InputBorder.none),
                            onChanged: onSearchTextChanged,
                          ),
                          trailing: showSearchClearIcon.value
                              ? IconButton(
                                  icon: const Icon(Icons.cancel),
                                  onPressed: () {
                                    controller.clear();
                                    onSearchTextChanged('');
                                  },
                                )
                              : null,
                        ),
                      ),
                      Expanded(
                        child: GestureDetector(
                          onTap: () => focusNode.unfocus(),
                          child: GridView.builder(
                              physics: const AlwaysScrollableScrollPhysics(),
                              gridDelegate:
                                  const SliverGridDelegateWithFixedCrossAxisCount(
                                      childAspectRatio: 1,
                                      crossAxisCount: 2,
                                      crossAxisSpacing: 5,
                                      mainAxisSpacing: 5),
                              itemCount: productsList.length,
                              itemBuilder: (context, position) {
                                return Utilities().customContainer(
                                    OpenContainer(
                                        closedColor: Colors.transparent,
                                        middleColor: Colors.transparent,
                                        openColor: Colors.transparent,
                                        transitionDuration:
                                            const Duration(milliseconds: 800),
                                        transitionType:
                                            ContainerTransitionType.fadeThrough,
                                        closedBuilder: (context, action) {
                                          return GestureDetector(
                                            onTap: () => {
                                              action(),
                                              focusNode.unfocus(),
                                            },
                                            child: Column(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.center,
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.center,
                                              children: [
                                                Utilities()
                                                    .clipImg(CachedNetworkImage(
                                                  height: MediaQuery.of(context)
                                                          .size
                                                          .height /
                                                      6,
                                                  width: MediaQuery.of(context)
                                                          .size
                                                          .width /
                                                      2.5,
                                                  imageUrl:
                                                      productsList[position]
                                                          .productImg
                                                          .toString(),
                                                  placeholder: (context, url) =>
                                                      const Center(
                                                          child:
                                                              ShowLoadingScreen(
                                                                  color: Colors
                                                                      .white)),
                                                )),
                                                Text(
                                                    productsList[position]
                                                        .productName
                                                        .toString()
                                                        .toTitleCase(),
                                                    style: const TextStyle(
                                                        color: Colors.white),
                                                    overflow:
                                                        TextOverflow.ellipsis,
                                                    maxLines: 1,
                                                    textScaleFactor: 1.5,
                                                    textAlign:
                                                        TextAlign.center),
                                              ],
                                            ),
                                          );
                                        },
                                        openBuilder: (context, action) {
                                          late Iterable<ProductCategoriesEntity>
                                              data;
                                          data = objectboxStore
                                              .box<ProductCategoriesEntity>()
                                              .getAll()
                                              .where((element) =>
                                                  element.catID ==
                                                  productsList[position]
                                                      .productCat
                                                      .target
                                                      ?.catID);
                                          if (data.isNotEmpty &&
                                              productsList[position]
                                                  .productCat
                                                  .target!
                                                  .catID!
                                                  .isNotEmpty) {
                                            return ProductDetailsScreen(
                                                catId: productsList[position]
                                                    .productCat
                                                    .target!
                                                    .catID
                                                    .toString(),
                                                productId:
                                                    productsList[position]
                                                        .productID!,
                                                productInfo: {
                                                  'catName': data.first.catName,
                                                  'productName':
                                                      productsList[position]
                                                          .productName,
                                                  'productImg':
                                                      productsList[position]
                                                          .productImg
                                                });
                                          } else {
                                            return const Text(
                                                'Empty Project details!');
                                          }
                                        }));
                              }),
                        ),
                      ),
                    ]));
          }
          return const ShowLoadingScreen(color: Colors.green);
        });
  }
}
