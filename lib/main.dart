import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:device_preview/device_preview.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_config/flutter_config.dart';
import 'package:get/get.dart';
import 'package:my_mart/firebase_options.dart';
import 'package:my_mart/otherFiles/AppTranslations.dart';
import 'package:my_mart/otherFiles/Routes.dart';
import 'package:my_mart/otherFiles/Screens/SplashScreen.dart';
import 'package:my_mart/services/AuthServices.dart';
import 'package:my_mart/utility/ObjectBoxEntities/AppData/EntityAppData.dart';
import 'package:my_mart/utility/Utilities.dart';

import 'objectbox.g.dart';

/// Provides access to the ObjectBox Store throughout the app.
late Store objectboxStore;
FirebaseAnalytics firebaseAnalytics = FirebaseAnalytics.instance;
FirebaseFirestore fireStore = FirebaseFirestore.instance;
final FirebaseAuth auth = FirebaseAuth.instance;

final navigatorKey = GlobalKey<NavigatorState>(debugLabel: 'navigatorKey');
final GlobalKey bottomNavigationGlobalKey =
    GlobalKey(debugLabel: 'btm_nav_bar');
FirebaseMessaging firebaseMessaging = FirebaseMessaging.instance;
final RxInt bottomNavSelectedIndex = 0.obs;

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await FlutterConfig.loadEnvVariables();
  debugPrint('ENVTxt');
  debugPrint('API : ${FlutterConfig.get('ENVTxt')}');
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );
  // await FirebaseAppCheck.instance.activate(
  //   webRecaptchaSiteKey: FlutterConfig.get('myAppCheckKey'),
  //   androidProvider: AndroidProvider.debug
  // );

  // // FirebaseCrashlytics.instance.crash();
  // if (kDebugMode) {
  //   // Force disable Crashlytics collection while doing every day development.
  //   // Temporarily toggle this to true if you want to test crash reporting in your app.
  //   await FirebaseCrashlytics.instance.setCrashlyticsCollectionEnabled(false);
  // } else {
  //   // Handle Crashlytics enabled status when not in Debug,
  //   // e.g. allow your users to opt-in to crash reporting.
  // }

  firebaseAnalytics.logAppOpen();
  if (kIsWeb) {
    await fireStore
        .enablePersistence(const PersistenceSettings(synchronizeTabs: true));
  } else {
    fireStore.settings = const Settings(
        persistenceEnabled: true,
        sslEnabled: true,
        cacheSizeBytes: Settings.CACHE_SIZE_UNLIMITED);
  }
  try {
    objectboxStore = await openStore();
  } catch (e) {
    debugPrint('E E E : $e');
    await Utilities().deleteWholeDataFromLocal();
    objectboxStore = await openStore();
  }
  var userUID = AuthServices().currentUser?.uid;
  if (userUID != null) {
    await firebaseAnalytics.setUserId(id: userUID);
  }
  Utilities().listenUserAuthAndFirebaseMessagingChanges();
  FirebaseMessaging.onBackgroundMessage(showNotificationFromFirebase);
  runApp(DevicePreview(
      enabled: !kReleaseMode, builder: (context) => const MyApp()));
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    var appDataBox = objectboxStore.box<EntityAppData>();
    var appData = appDataBox.getAll();
    if (appData.isEmpty) {
      appDataBox.put(EntityAppData(userCurrentLanguage: 'hi'));
      appData = appDataBox.getAll();
    }
    var firstAppData = appData.first;
    return GetMaterialApp(
        translations: AppTranslations(),
        locale: Locale(firstAppData.userCurrentLanguage,
            firstAppData.userCurrentLanguage != 'en' ? 'IN' : 'US'),
        fallbackLocale: const Locale('en', 'US'),
        title: 'My Mart',
        theme: ThemeData(
            appBarTheme: const AppBarTheme(
              backgroundColor: Colors.green,
              foregroundColor: Colors.white,
            ),
            scaffoldBackgroundColor: Colors.white,
            useMaterial3: true,
            colorScheme: ColorScheme.fromSwatch()
                .copyWith(primary: Colors.green, secondary: Colors.green),
            pageTransitionsTheme: const PageTransitionsTheme(
              builders: <TargetPlatform, PageTransitionsBuilder>{
                TargetPlatform.android: FadeUpwardsPageTransitionsBuilder(),
              },
            ),
            cardColor: Colors.white),
        routes: routesList,
        navigatorKey: navigatorKey,
        useInheritedMediaQuery: true,
        builder: DevicePreview.appBuilder,
        darkTheme: ThemeData.dark(
          useMaterial3: true,
        ).copyWith(
          primaryColor: Colors.grey,
          textSelectionTheme: const TextSelectionThemeData(
              cursorColor: Colors.grey,
              selectionColor: Colors.grey,
              selectionHandleColor: Colors.grey),
          inputDecorationTheme: InputDecorationTheme(
            enabledBorder: const OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(20)),
              borderSide: BorderSide(width: 3, color: Colors.white24),
            ),
            focusedBorder: const OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(4)),
              borderSide: BorderSide(width: 3, color: Colors.white24),
            ),
            errorBorder: OutlineInputBorder(
              borderRadius: const BorderRadius.all(Radius.circular(20)),
              borderSide: BorderSide(width: 3, color: Colors.red.shade300),
            ),
          ),
        ),
        home: const SplashScreen());
  }
}
