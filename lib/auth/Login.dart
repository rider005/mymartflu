import 'dart:io';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:my_mart/otherFiles/Constants.dart';
import 'package:my_mart/otherFiles/Screens/ShowLoadingScreen.dart';
import 'package:my_mart/services/AuthServices.dart';
import 'package:my_mart/utility/Utilities.dart';

import '../main.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  LoginScreenState createState() => LoginScreenState();
}

class LoginScreenState extends State<LoginScreen> {
  final _formKey = GlobalKey<FormState>();

  RxString userName = ''.obs,
      userEmail = ''.obs,
      userPass = ''.obs,
      userConPass = ''.obs;
  RxBool isLoading = false.obs,
      isLogin = true.obs,
      isPasswordWrong = false.obs,
      passwordVisible = false.obs;

  Offset distance = const Offset(28, 28);
  double blur = 30.0;
  var colWidth = 0.40;

  _onLoginBtnClickHandler() async {
    if (_formKey.currentState!.validate()) {
      FocusScope.of(context).unfocus();
      try {
        isLoading.value = true;
        await AuthServices().handleSignInEmail(
            null, userEmail.value.toLowerCase().trim(), userPass.value.trim());
        // if ((await user.getIdTokenResult(true)).claims!.containsKey('isUser')) {
        //   await firebaseAnalytics.setUserId(id: user.uid);
        //   await firebaseAnalytics.logLogin(loginMethod: 'email');
        //   isLoading.value = false;
        //   Utilities().showSnackBar('Login Successful'.tr);
        //   navigateToDashBoard();
        // } else {
        //   isLoading.value = false;
        //   try {
        //     await AuthServices().logOutUser();
        //   } catch (e) {
        //     print(e);
        //   }
        // }
        isLoading.value = false;
        navigateToDashBoard();
        Utilities().showSnackBar('Login Successful'.tr);
      } catch (e) {
        debugPrint('Auth E : $e');
        if (e.toString().contains('There is no user')) {
          if (isLogin.value) {
            // setState(() => {
            isLogin.value = false;
            isLoading.value = false;
            userPass.value = "";
            isPasswordWrong.value = true;
            // });
            Utilities().showSnackBar('Try to register your-self');
            return;
          } else {
            try {
              Utilities().startScreenTracking('SignUp');
              (await AuthServices().handleSignUp(
                  null, userName.value, userEmail.value, userPass.value));
              isLoading.value = false;
              navigateToDashBoard();
              Utilities().showSnackBar('SignUp Success');
            } catch (e) {
              if (kDebugMode) {
                print(e);
              }
              isLoading.value = false;
              Utilities().showSnackBar('something wrong, try again');
            }
          }
        } else if (e.toString().contains('password is invalid')) {
          Utilities().showSnackBar(!isPasswordWrong.value
              ? 'Enter Your Password'
              : 'password is invalid, try forget-password...');
          isLoading.value = false;
          isPasswordWrong.value = true;
          userPass.value = "";
        }
        isLoading.value = false;
        Utilities().showSnackBar('Try again later!');
      }
    } else {
      Utilities().showSnackBar('Enter valid data');
    }
  }

  navigateToDashBoard() {
    Navigator.of(navigatorKey.currentContext!).pushNamedAndRemoveUntil(
        DASHBOARD_SCREEN, (Route<dynamic> route) => false);
  }

  _googleSignInHandler() async {
    try {
      isLoading.value = true;
      debugPrint('GOOGLE SIGN IN');
      var googleUser = await GoogleSignIn().signIn();
      var googleAuth = await googleUser!.authentication;
      var credential = GoogleAuthProvider.credential(
          accessToken: googleAuth.accessToken, idToken: googleAuth.idToken);
      var signInWithCredential = await auth.signInWithCredential(credential);
      var user = signInWithCredential.user;
      if (user != null) {
        debugPrint(
            'signInWithCredential.additionalUserInfo?.isNewUser : ${signInWithCredential.additionalUserInfo?.isNewUser}');
        if (signInWithCredential.additionalUserInfo?.isNewUser == true) {
          debugPrint('Sign-Up');
          await AuthServices().handleSignUp(
              user, user.displayName.toString(), user.email.toString());
        } else {
          debugPrint('Sign-IN');
          await AuthServices().handleSignInEmail(user);
        }
        isLoading.value = false;
        navigateToDashBoard();
        Utilities().showSnackBar('Success');
      }
    } catch (error) {
      isLoading.value = false;
      debugPrint(error.toString());
      Utilities().showSnackBar('Try again later!');
    }
  }

  _onForgetPass() {
    AuthServices()
        .forgetPass(userEmail.value)
        .then((value) =>
            {Utilities().showSnackBar('Forget password email send success')})
        .catchError((onError) =>
            {Utilities().showSnackBar('Write valid email or Try again')});
  }

  @override
  void initState() {
    super.initState();
    // setState(() {
    Utilities().startScreenTracking('SignIn');
    if (kIsWeb) {
      //running on web
      colWidth = 0.40;
    } else if (Platform.isAndroid || Platform.isIOS) {
      //running on android or ios device
      colWidth = 0.80;
    }
    isLogin.value = true;
    // userEmail.value = 'devenchavda1088@gmail.com';
    // userPass.value = 'Admin@123';
    if (kDebugMode) {
      userEmail.value = 'devenchavda1088@gmail.com';
      userPass.value = 'Admin@123';
    } else {
      userPass.value = '123456';
    }
    passwordVisible.value = false;
    // });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Obx(() => Text(isLogin.value ? 'Login'.tr : 'Register'.tr)),
      ),
      resizeToAvoidBottomInset: true,
      body: AnimatedSwitcher(
          duration: const Duration(seconds: 1),
          child: Obx(() => !isLoading.value
              ? Center(
                  child: SingleChildScrollView(
                    child: Form(
                      key: _formKey,
                      child: Center(
                        child: FractionallySizedBox(
                            widthFactor: colWidth, // between 0 and 1
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                !isLogin.value
                                    ? TextFormField(
                                        decoration: const InputDecoration(
                                          border: UnderlineInputBorder(),
                                          labelText: 'Enter your name',
                                          hintText: 'Enter name here',
                                        ),
                                        textAlign: TextAlign.center,
                                        initialValue: userName.value,
                                        onChanged: (userNameChange) => setState(
                                            () => userName.value =
                                                userNameChange),
                                        validator: (name) =>
                                            Utilities().validateName(name!),
                                        autovalidateMode:
                                            AutovalidateMode.onUserInteraction)
                                    : Container(),
                                TextFormField(
                                    decoration: const InputDecoration(
                                      border: UnderlineInputBorder(),
                                      labelText: 'Enter your email',
                                      hintText: 'Enter email here',
                                    ),
                                    textAlign: TextAlign.center,
                                    initialValue: userEmail.value,
                                    onChanged: (userEmailChange) => setState(
                                        () =>
                                            userEmail.value = userEmailChange),
                                    validator: (email) =>
                                        Utilities().validateEmail(email!),
                                    autovalidateMode:
                                        AutovalidateMode.onUserInteraction),
                                isPasswordWrong.value
                                    ? TextFormField(
                                        decoration: InputDecoration(
                                          border: const UnderlineInputBorder(),
                                          labelText: 'Enter your password',
                                          hintText: 'Enter password here',
                                          suffixIcon: IconButton(
                                            icon: Icon(passwordVisible.value
                                                ? Icons.visibility
                                                : Icons.visibility_off),
                                            onPressed: () {
                                              setState(() {
                                                passwordVisible.value =
                                                    !passwordVisible.value;
                                              });
                                            },
                                          ),
                                        ),
                                        textAlign: TextAlign.center,
                                        obscureText: !passwordVisible.value,
                                        initialValue: userPass.value,
                                        onChanged: (userPassChange) => setState(
                                            () => userPass.value =
                                                userPassChange),
                                        validator: (pass) =>
                                            Utilities().validatePass(pass!),
                                        autovalidateMode:
                                            AutovalidateMode.onUserInteraction)
                                    : Container(),
                                !isLogin.value
                                    ? TextFormField(
                                        decoration: const InputDecoration(
                                          border: UnderlineInputBorder(),
                                          labelText:
                                              'Enter your confirm password',
                                          hintText:
                                              'Enter confirm password here',
                                          suffixIcon: Icon(Icons.lock),
                                        ),
                                        textAlign: TextAlign.center,
                                        initialValue: userConPass.value,
                                        onChanged: (userConPassChange) =>
                                            setState(() => userConPass.value =
                                                userConPassChange),
                                        validator: (conPass) {
                                          if (userPass == userConPass) {
                                            Utilities().validatePass(conPass!);
                                          } else {
                                            return 'password and confirm password is not same';
                                          }
                                        },
                                        autovalidateMode:
                                            AutovalidateMode.onUserInteraction)
                                    : Container(),
                                isLogin.value
                                    ? Align(
                                        alignment: Alignment.topRight,
                                        child: GestureDetector(
                                            onTap: () => _onForgetPass(),
                                            child:
                                                const Text('Forget Password')))
                                    : Align(
                                        alignment: Alignment.topRight,
                                        child: GestureDetector(
                                            onTap: () {
                                              isLogin.value = true;
                                            },
                                            child:
                                                const Text('Want to login?'))),
                                OutlinedButton(
                                  onPressed: _onLoginBtnClickHandler,
                                  child: Text(isLogin.value
                                      ? 'Login'.tr
                                      : 'Register'.tr),
                                ),
                                InkWell(
                                  onTap: _googleSignInHandler,
                                  child: Container(
                                    height: 50,
                                    width: 200,
                                    color: Colors.blueAccent,
                                    child: Row(
                                      children: [
                                        Container(
                                          margin: const EdgeInsets.symmetric(
                                              horizontal: 3),
                                          height: 40,
                                          width: 45,
                                          decoration: const BoxDecoration(
                                              color: Colors.white,
                                              image: DecorationImage(
                                                  image: NetworkImage(
                                                      "https://cdn-icons-png.flaticon.com/512/2991/2991148.png"))),
                                        ),
                                        const SizedBox(
                                          width: 10,
                                        ),
                                        const Text(
                                          "SignUp With Google",
                                          style: TextStyle(
                                            color: Colors.white,
                                            fontWeight: FontWeight.bold,
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                              ]
                                  .map((e) => Padding(
                                        padding: const EdgeInsets.symmetric(
                                            vertical: 10),
                                        child: e,
                                      ))
                                  .toList(),
                            )),
                      ),
                    ),
                  ),
                )
              : const ShowLoadingScreen(color: Colors.green))),
    );
  }
}
