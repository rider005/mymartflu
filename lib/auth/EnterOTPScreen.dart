import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:my_mart/main.dart';
import 'package:my_mart/otherFiles/Screens/ShowLoadingScreen.dart';
import 'package:my_mart/services/FireStoreServices/UserServices.dart';

import '../utility/Utilities.dart';

class EnterOTPScreen extends StatefulWidget {
  final String userPhone;

  const EnterOTPScreen({Key? key, required this.userPhone}) : super(key: key);

  @override
  State<EnterOTPScreen> createState() => _EnterOTPScreenState();
}

class _EnterOTPScreenState extends State<EnterOTPScreen> {
  // 4 text editing controllers that associate with the 4 input fields
  final TextEditingController _fieldOne = TextEditingController(),
      _fieldTwo = TextEditingController(),
      _fieldThree = TextEditingController(),
      _fieldFour = TextEditingController(),
      _fieldFive = TextEditingController(),
      _fieldSix = TextEditingController();

  // This is the entered code
  // It will be displayed in a Text widget
  final RxString _otp = ''.obs, _verificationId = ''.obs;
  RxBool isLoading = true.obs;

  sendOTP() {
    isLoading.value = true;
    auth.verifyPhoneNumber(
      phoneNumber: '+91 ${widget.userPhone}',
      timeout: const Duration(minutes: 2),
      verificationCompleted: (PhoneAuthCredential credential) async {
        if (mounted) {
          debugPrint('credential : ${credential.smsCode}');
          debugPrint('verify Complete Auto');
          Utilities().showSnackBar('Verifying OTP done');
          await UserServices()
              .updateCurrentUserPhone(widget.userPhone, credential);
          isLoading.value = false;
          Navigator.pop(navigatorKey.currentState!.context);
        }
      },
      verificationFailed: (FirebaseAuthException e) {
        if (mounted) {
          isLoading.value = false;
          if (e.code == 'invalid-phone-number') {
            Utilities().showSnackBar('The provided phone number is not valid.');
          }
          debugPrint('verificationFailed : ${e.message}');
          // else {
          //   Utilities().showSnackBar(e.message!);
          // }
        }
      },
      codeSent: (String verificationId, int? resendToken) {
        if (mounted) {
          isLoading.value = false;
          _verificationId.value = verificationId;
          Utilities().showSnackBar('OTP is sent!');
        }
      },
      codeAutoRetrievalTimeout: (String verificationId) {
        if (mounted) {
          isLoading.value = false;
          Utilities().showSnackBar('Timeout, Try again later!');
        }
      },
    );
  }

  @override
  void initState() {
    super.initState();
    sendOTP();
    isLoading.value = false;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('KindaCode'),
      ),
      body: Obx(() => AnimatedSwitcher(
            duration: const Duration(seconds: 1),
            child: isLoading.value
                ? const ShowLoadingScreen(color: Colors.green)
                : Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const Text('Enter OTP Below!'),
                      const SizedBox(
                        height: 30,
                      ),
                      // Implement 4 input fields
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          OtpInput(_fieldOne, true),
                          OtpInput(_fieldTwo, false),
                          OtpInput(_fieldThree, false),
                          OtpInput(_fieldFour, false),
                          OtpInput(_fieldFive, false),
                          OtpInput(_fieldSix, false)
                        ],
                      ),
                      const SizedBox(
                        height: 30,
                      ),
                      ElevatedButton(
                          onPressed: () {
                            _otp.value = _fieldOne.text +
                                _fieldTwo.text +
                                _fieldThree.text +
                                _fieldFour.text +
                                _fieldFive.text +
                                _fieldSix.text;
                            _verifyOTP();
                          },
                          child: const Text('Submit')),
                      const SizedBox(
                        height: 20,
                      ),
                      GestureDetector(
                          onTap: sendOTP,
                          child: const Text(
                            'Resend OTP!',
                            style: TextStyle(fontSize: 25),
                          ))
                    ],
                  ),
          )),
    );
  }

  _verifyOTP() async {
    if (_verificationId.value.length > 1 && _otp.value.length == 6) {
      // Create a PhoneAuthCredential with the code
      PhoneAuthCredential credential = PhoneAuthProvider.credential(
          verificationId: _verificationId.value, smsCode: _otp.value);
      isLoading.value = true;
      try {
        await UserServices()
            .updateCurrentUserPhone(widget.userPhone, credential);
        isLoading.value = false;
        Navigator.pop(navigatorKey.currentContext!);
        // debugPrint('user is deleted after verify phone number!');
      } on FirebaseAuthException catch (e) {
        isLoading.value = false;
        debugPrint('E message : ${e.message!}');
        if (e.message!.contains('Please resend the verification code')) {
          Utilities().showSnackBar('Enter valid OTP!');
        }
      }
    }
  }
}

// Create an input widget that takes only one digit
class OtpInput extends StatelessWidget {
  final TextEditingController controller;
  final bool autoFocus;

  const OtpInput(this.controller, this.autoFocus, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 60,
      width: 50,
      child: TextField(
        autofocus: autoFocus,
        textAlign: TextAlign.center,
        keyboardType: TextInputType.number,
        controller: controller,
        maxLength: 1,
        cursorColor: Theme.of(context).primaryColor,
        decoration: const InputDecoration(
            border: OutlineInputBorder(),
            counterText: '',
            hintStyle: TextStyle(color: Colors.black, fontSize: 20.0)),
        onChanged: (value) {
          if (value.length == 1) {
            FocusScope.of(context).nextFocus();
          }
        },
      ),
    );
  }
}
