import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:my_mart/auth/EnterOTPScreen.dart';
import 'package:my_mart/main.dart';
import 'package:my_mart/otherFiles/Screens/ShowLoadingScreen.dart';
import 'package:my_mart/services/AuthServices.dart';
import 'package:my_mart/services/FireStoreServices/UserServices.dart';
import 'package:my_mart/utility/Utilities.dart';

import '../objectbox.g.dart';
import '../otherFiles/Constants.dart';
import '../utility/ObjectBoxEntities/User/UserEntity.dart';

class Profile extends StatefulWidget {
  const Profile({Key? key}) : super(key: key);

  @override
  State<Profile> createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  RxBool isLoading = false.obs,
      wantToUpdate = false.obs;

  late final Stream<List<UserEntity>> _userDetailsStream;
  RxString userNameChanges = ''.obs,
      userPhoneChanges = ''.obs,
      userAddressChanges = ''.obs,
      creatingDate = ''.obs,
      creatingTime = ''.obs,
      lastSignInDate = ''.obs,
      lastSignInTime = ''.obs;
  var colWidth = 0.40;

  _updateOrSaveUserProfile() async {
    if (!wantToUpdate.value) {
      wantToUpdate.value = true;
    } else {
      if (kDebugMode) {
        print("let's update your profile");
      }
      try {
        isLoading.value = true;
        var user = objectboxStore
            .box<UserEntity>()
            // The simplest possible query that just gets ALL the data out of the Box
            .query(UserEntity_.userUID.equals(AuthServices().currentUser!.uid))
            .build()
            .findFirst();
        if (user!.userName != userNameChanges.value &&
            userNameChanges.value.isNotEmpty) {
          await UserServices().updateCurrentUser(userNameChanges.value);
        }
        if (user.userAddress != userAddressChanges.value &&
            userAddressChanges.value.isNotEmpty) {
          await UserServices()
              .getCurrentUserRef(user.userUID!)
              .update({'userAddress': userAddressChanges.value});
          user.userAddress = userAddressChanges.value;
          objectboxStore.box<UserEntity>().put(user, mode: PutMode.update);
        }
        if ((userPhoneChanges.value.isNotEmpty &&
                userPhoneChanges.value.length == 10 &&
                userPhoneChanges.value !=
                    AuthServices().currentUser?.phoneNumber) ||
            ('+91${user.userPhone}' !=
                AuthServices().currentUser?.phoneNumber)) {
          Navigator.push(
              navigatorKey.currentContext!,
              MaterialPageRoute(
                  builder: (BuildContext context) =>
                      EnterOTPScreen(userPhone: userPhoneChanges.value)));
        }
        isLoading.value = false;
        wantToUpdate.value = false;
      } catch (e) {
        if (kDebugMode) {
          print(e);
        }
        Utilities().showSnackBar('Try again later!');
        isLoading.value = false;
        wantToUpdate.value = false;
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    setDateAndTimeInCurrentLen();
    getProfileDetails(List<UserEntity> userDetails) {
      return Obx(() =>
          Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                !wantToUpdate.value
                    ? Text(
                  userDetails.first.userName ?? 'Update Your Name',
                  style: const TextStyle(
                      fontSize: 25.0,
                        color: Colors.blueGrey,
                        letterSpacing: 2.0,
                        fontWeight: FontWeight.w400),
                  )
                : FractionallySizedBox(
                    widthFactor: colWidth,
                    child: TextFormField(
                        decoration: const InputDecoration(
                          border: UnderlineInputBorder(),
                          labelText: 'Enter your name',
                          hintText: 'Enter name here',
                        ),
                        textAlign: TextAlign.center,
                        initialValue: userDetails.first.userName,
                        onChanged: (userName) =>
                            userNameChanges.value = userName,
                        validator: (name) => Utilities().validateName(name!),
                        autovalidateMode: AutovalidateMode.onUserInteraction),
                  ),
            Text(
              userDetails.first.userEmail ?? '',
              style: const TextStyle(
                  fontSize: 18.0,
                  color: Colors.black45,
                  letterSpacing: 2.0,
                  fontWeight: FontWeight.w300),
            ),
                if (!wantToUpdate.value)
                  Text(
                    userDetails.first.userAddress!.isEmpty
                        ? 'Update Your Address'
                        : userDetails.first.userAddress!,
                    style: const TextStyle(
                        fontSize: 18.0,
                        color: Colors.black45,
                        letterSpacing: 2.0,
                        fontWeight: FontWeight.w300),
                  )
            else
              FractionallySizedBox(
                widthFactor: colWidth,
                child: TextFormField(
                    maxLength: 150,
                    minLines: 1,
                    maxLines: 5,
                    decoration: const InputDecoration(
                      border: UnderlineInputBorder(),
                      labelText: 'Enter Your Address',
                      hintText: 'Enter Your Address Here',
                    ),
                    textAlign: TextAlign.center,
                    keyboardType: TextInputType.streetAddress,
                    initialValue: userDetails.last.userAddress,
                    onChanged: (userAddress) =>
                        userAddressChanges.value = userAddress,
                    validator: (address) =>
                        Utilities().validateAddress(address!),
                    autovalidateMode: AutovalidateMode.onUserInteraction),
              ),
            Text(
              '${'Email verified'.tr} : ${userDetails.first.userEmailVerified
                  .toString()
                  .tr}',
              style: const TextStyle(
                  fontSize: 18.0,
                  color: Colors.black45,
                  letterSpacing: 2.0,
                  fontWeight: FontWeight.w300),
            ),
            if ('${userDetails.first.userPhone}'.length > 1 &&
                !wantToUpdate.value)
              Text(
                Utilities().translateLongNumber(
                    userDetails.first.userPhone.toString()),
                style: const TextStyle(
                    fontSize: 18.0,
                    color: Colors.black45,
                    letterSpacing: 2.0,
                    fontWeight: FontWeight.w300),
              )
            else
              wantToUpdate.value
                  ? FractionallySizedBox(
                      widthFactor: colWidth,
                      child: TextFormField(
                          maxLength: 10,
                          decoration: const InputDecoration(
                            border: UnderlineInputBorder(),
                            labelText: 'Enter your phone number',
                            hintText: 'Enter phone number here',
                          ),
                          textAlign: TextAlign.center,
                          keyboardType: TextInputType.number,
                          initialValue: userDetails.last.userPhone.toString(),
                          onChanged: (userPhone) =>
                              userPhoneChanges.value = userPhone,
                          validator: (phone) =>
                              Utilities().validatePhone(phone!),
                          autovalidateMode: AutovalidateMode.onUserInteraction),
                    )
                  : Container(),
            Text(
              '${'Phone number verified'.tr} : ${userDetails.first
                  .userPhoneVerified
                  .toString()
                  .tr}',
              style: const TextStyle(
                  fontSize: 18.0,
                  color: Colors.black45,
                  letterSpacing: 2.0,
                  fontWeight: FontWeight.w300),
            ),
            Text(
              '${'Creation Date'.tr} : $creatingDate',
              style: const TextStyle(
                  fontSize: 15.0,
                  color: Colors.black45,
                  letterSpacing: 2.0,
                  fontWeight: FontWeight.w300),
            ),
            Text(
              '${'Creation Time'.tr} : ${Utilities().translateLongNumber(
                  creatingTime.value)}',
              style: const TextStyle(
                  fontSize: 15.0,
                  color: Colors.black45,
                  letterSpacing: 2.0,
                  fontWeight: FontWeight.w300),
            ),
            Text(
              '${'Last SignIn Day'.tr} : $lastSignInDate',
              style: const TextStyle(
                  fontSize: 15.0,
                  color: Colors.black45,
                  letterSpacing: 2.0,
                  fontWeight: FontWeight.w300),
            ),
            Text(
              '${'Last SignIn Time'.tr} : ${Utilities().translateLongNumber(
                  lastSignInTime.value)}',
              style: const TextStyle(
                  fontSize: 15.0,
                  color: Colors.black45,
                  letterSpacing: 2.0,
                  fontWeight: FontWeight.w300),
            ),
          ]
              .map((e) => Padding(
                    padding: const EdgeInsets.symmetric(vertical: 2),
                    child: e,
                  ))
              .toList()));
    }

    return StreamBuilder(
      stream: _userDetailsStream,
      builder:
          (BuildContext context, AsyncSnapshot<List<UserEntity>> snapshot) {
            Widget currentWidget = const ShowLoadingScreen(color: Colors.white);
            if (ConnectionState.active == snapshot.connectionState &&
                snapshot.hasData) {
              var userDetails = snapshot.data as List<UserEntity>;
              if (userDetails.isNotEmpty) {
                currentWidget = SingleChildScrollView(
                  child: Column(
                    children: [
                      const Padding(
                        padding: EdgeInsets.symmetric(vertical: 10),
                        child: CircleAvatar(
                          backgroundImage: NetworkImage(
                              "https://cdn-images-1.medium.com/max/1200/1*5-aoK8IBmXve5whBQM90GA.png"),
                          radius: 60.0,
                        ),
                      ),
                      Utilities().customCard(
                          true,
                          Container(
                            margin: const EdgeInsets.symmetric(
                                horizontal: 5.0, vertical: 8.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Column(
                                  children: [
                                    Text(
                                      "Completed".tr,
                                      style: const TextStyle(
                                          color: Colors.white,
                                          fontSize: 22.0,
                                          fontWeight: FontWeight.w600),
                                    ),
                                    const SizedBox(
                                      height: 7,
                                    ),
                                    Text(
                                      '${Utilities().translateNumber(
                                          userDetails.first.userCompletedOrders
                                              .toString())}',
                                      style: const TextStyle(
                                          color: Colors.white,
                                          fontSize: 22.0,
                                          fontWeight: FontWeight.w300),
                                    ),
                                  ],
                                ),
                                Column(
                                  children: [
                                    Text(
                                      "Pending".tr,
                                      style: const TextStyle(
                                          color: Colors.white,
                                          fontSize: 22.0,
                                          fontWeight: FontWeight.w600),
                                    ),
                                    const SizedBox(
                                      height: 7,
                                    ),
                                    Text(
                                      '${Utilities().translateNumber(
                                          userDetails.first.userPendingOrders
                                              .toString())}',
                                      style: const TextStyle(
                                          color: Colors.white,
                                          fontSize: 22.0,
                                          fontWeight: FontWeight.w300),
                                    )
                                  ],
                                ),
                                Column(
                                  children: [
                                    Text(
                                      "In Cart".tr,
                                      style: const TextStyle(
                                          color: Colors.white,
                                          fontSize: 22.0,
                                          fontWeight: FontWeight.w600),
                                    ),
                                    const SizedBox(
                                      height: 7,
                                    ),
                                    Text(
                                      '${Utilities().translateNumber(
                                          userDetails.first.userCart
                                              .toString())}',
                                      style: const TextStyle(
                                          color: Colors.white,
                                          fontSize: 22.0,
                                          fontWeight: FontWeight.w300),
                                    )
                                  ],
                                ),
                              ],
                            ),
                          ),
                          color: Colors.green),
                      FractionallySizedBox(
                          widthFactor: 0.9,
                          child: getProfileDetails(userDetails)),
                      Obx(() =>
                          OutlinedButton(
                            style: ButtonStyle(
                              backgroundColor:
                              MaterialStateProperty.all(Colors.green),
                            ),
                            onPressed: _updateOrSaveUserProfile,
                            child: Text(
                              !wantToUpdate.value
                                  ? 'Update Your Profile'.tr
                                  : 'Save Profile'.tr,
                          style: const TextStyle(color: Colors.white),
                        ),
                      )),
                  OutlinedButton(
                    style: ButtonStyle(
                      backgroundColor: MaterialStateProperty.all(Colors.green),
                    ),
                    onPressed: _logOutUserHandler,
                    child: Text(
                      'Logout'.tr,
                      style: const TextStyle(color: Colors.white),
                    ),
                  )
                    ],
                  ),
                );
              }
            }
            return AnimatedSwitcher(
              duration: const Duration(seconds: 1),
              child: currentWidget,
            );
      },
    );
  }

  _logOutUserHandler() async {
    isLoading.value = true;
    await AuthServices().logOutUser();
    isLoading.value = false;
    Navigator.of(navigatorKey.currentContext!)
        .pushNamedAndRemoveUntil(LOGIN_SCREEN, (Route<dynamic> route) => false);
  }

  setDateAndTimeInCurrentLen() {
    var creationTimeFromAuth =
    AuthServices().currentUser?.metadata.creationTime!;
    creatingDate.value =
    '${Utilities().translateNumber(creationTimeFromAuth!.day.toString())} '
        '${Utilities().getMonthName(creationTimeFromAuth.month)} '
        '${Utilities().translateLongNumber(
        creationTimeFromAuth.year.toString())}';
    creatingTime.value =
    '${creationTimeFromAuth.hour}:${creationTimeFromAuth.minute}';
    var lastSignInTimeFromAuth =
    AuthServices().currentUser?.metadata.lastSignInTime!;
    lastSignInDate.value =
    '${Utilities().translateLongNumber(
        lastSignInTimeFromAuth!.day.toString())} '
        '${Utilities().getMonthName(lastSignInTimeFromAuth.month)} '
        '${Utilities().translateLongNumber(
        lastSignInTimeFromAuth.year.toString())}';
    lastSignInTime.value =
    '${lastSignInTimeFromAuth.hour}:${lastSignInTimeFromAuth.minute}';
  }

  @override
  void initState() {
    _userDetailsStream = objectboxStore
        .box<UserEntity>()
    // The simplest possible query that just gets ALL the data out of the Box
        .query(UserEntity_.userUID.equals(AuthServices().currentUser!.uid))
        .watch(triggerImmediately: true)
    // Watching the query produces a Stream<Query<ProductCategories>>
    // To get the actual data inside a List<ProductCategories>, we need to call find() on the query
        .map((query) => query.find());
    UserServices().listenUserDataChange();
    isLoading.value = false;
    if (kIsWeb) {
      //running on web
      colWidth = 0.40;
    } else if (Platform.isAndroid || Platform.isIOS) {
      //running on android or ios device
      colWidth = 0.60;
    }
    super.initState();
  }
}
