import 'dart:io';

import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:my_mart/main.dart';
import 'package:my_mart/products/AllCategories.dart';
import 'package:my_mart/products/ProductsTabs.dart';
import 'package:my_mart/profile/Profile.dart';
import 'package:my_mart/services/FireStoreServices/ProductServices.dart';
import 'package:my_mart/utility/ObjectBoxEntities/AppData/EntityAppData.dart';
import 'package:my_mart/utility/ObjectBoxEntities/User/UserEntity.dart';
import 'package:translator/translator.dart';

import 'DashboardScreen.dart';

class BottomNavigationScreen extends StatefulWidget {
  const BottomNavigationScreen({Key? key}) : super(key: key);

  @override
  State<BottomNavigationScreen> createState() => _MyBottomNavStatefulWidget();
}

class _MyBottomNavStatefulWidget extends State<BottomNavigationScreen> {
  static const TAB_NAME = ['Dashboard', 'allCatLbl', 'Orders', 'Profile'];
  int divideHeightBy = 0;
  FocusNode dropdown = FocusNode();
  var lanList = ['ગુજરાતી', 'हिन्दी', 'English'];
  final RxString _navTitle = TAB_NAME[0].obs, selectedValue = 'ગુજરાતી'.obs;
  static final List<Widget> _widgetOptions = <Widget>[
    const DashboardScreen(),
    const AllCategoriesScreen(),
    const ProductsOrder(),
    const Profile()
  ];

  void onItemTapped(int index) {
    _navTitle.value = TAB_NAME[index];
    bottomNavSelectedIndex.value = index;
  }

  @override
  initState() {
    if (kIsWeb) {
      //running on web
      divideHeightBy = 2;
    } else if (Platform.isAndroid || Platform.isIOS) {
      //running on android or ios device
      divideHeightBy = 4;
    }
    if (Get.locale.toString().substring(0, 2) == 'gu') {
      selectedValue.value = lanList[0];
    } else if (Get.locale.toString().substring(0, 2) == 'hi') {
      selectedValue.value = lanList[1];
    } else {
      selectedValue.value = lanList[2];
    }

    ProductServices().listenProductsCatListChanges();
    ProductServices().listenWishListChanges();
    ProductServices().listenCartListChanges();
    ProductServices().listenOrdersListChanges();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Obx(() => Scaffold(
          appBar: AppBar(
            title: Text(_navTitle.value.tr,
                style: const TextStyle(color: Colors.white)),
            actions: [
              Container(
                padding:
                    const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                // decoration: BoxDecoration(
                //     color: Colors.white, borderRadius: BorderRadius.circular(10)
                // ),
                child: DropdownButton<String>(
                  focusNode: dropdown,
                  value: selectedValue.value,
                  elevation: 16,
                  dropdownColor: Colors.green,
                  onChanged: (newValue) {
                    updateAppLocalLan(newValue);
                    selectedValue.value = newValue!;
                  },
                  items: lanList.map<DropdownMenuItem<String>>((listItem) {
                    return DropdownMenuItem<String>(
                      value: listItem,
                      child: GestureDetector(
                          child: Text(
                        listItem,
                        style: const TextStyle(color: Colors.white),
                      )),
                    );
                  }).toList(),
                  icon: const Icon(Icons.arrow_drop_down),
                  iconSize: 42,
                  underline: const SizedBox(),
                ),
              )
            ],
          ),
          body: _widgetOptions.elementAt(bottomNavSelectedIndex.value),
          bottomNavigationBar: CurvedNavigationBar(
            height: 50,
            backgroundColor: Colors.transparent,
            key: bottomNavigationGlobalKey,
            items: const [
              Icon(
                Icons.home,
                color: Colors.white,
              ),
              Icon(Icons.business, color: Colors.white),
              Icon(Icons.school, color: Colors.white),
              Icon(Icons.account_circle, color: Colors.white),
            ],
            index: bottomNavSelectedIndex.value,
            // selectedItemColor: Colors.amber[800],
            onTap: onItemTapped,
            color: Get.isDarkMode ? Colors.black54 : Colors.green,
            animationCurve: Curves.slowMiddle,
            // animationDuration: const Duration(milliseconds: 350),
            // buttonBackgroundColor: Colors.white,
          ),
        ));
  }

  void updateAppLocalLan(lanName) {
    dropdown.unfocus();
    var appDataBox = objectboxStore.box<EntityAppData>();
    var appData = appDataBox.getAll().first;
    switch (lanName) {
      case 'ગુજરાતી':
        Get.updateLocale(const Locale('gu', 'IN'));
        appData.userCurrentLanguage = 'gu';
        break;
      case 'हिन्दी':
        Get.updateLocale(const Locale('hi', 'IN'));
        appData.userCurrentLanguage = 'hi';
        break;
      case 'English':
        Get.updateLocale(const Locale('en', 'US'));
        appData.userCurrentLanguage = 'en';
        break;
      default:
        Get.updateLocale(const Locale('hi', 'IN'));
        appData.userCurrentLanguage = 'hi';
        break;
    }
    appDataBox.put(appData);
    if (appData.userCurrentLanguage != 'en') {
      try {
        var userBox = objectboxStore.box<UserEntity>();
        var userData = userBox.getAll().first;
        GoogleTranslator()
            .translate(userData.userName.toString(),
                to: appData.userCurrentLanguage.toString())
            .then((value) =>
                {userData.userName = value.text, userBox.put(userData)});
      } catch (e) {
        debugPrint('E User is not there or CATCH : $e');
      }
    }
  }
}
