import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:my_mart/objectbox.g.dart';
import 'package:my_mart/services/AuthServices.dart';
import 'package:my_mart/services/FireStoreServices/FirestoreServices.dart';

import '../../main.dart';
import '../../utility/ObjectBoxEntities/ProductCat/EntityForProducts.dart';
import '../../utility/Utilities.dart';

class ProductServices {
  Future listenProductsCatListChanges() async {
    // final translator = GoogleTranslator();
    Stream<QuerySnapshot> productCatCollChange =
        FirestoreServices().productCategoriesCollRef.snapshots();

    Box<ProductCategoriesEntity> box =
        objectboxStore.box<ProductCategoriesEntity>();
    productCatCollChange.forEach((col) async {
      for (var docs in col.docChanges) {
        var isDocExists =
            box.getAll().where((element) => element.catID == docs.doc.id);
        switch (docs.type) {
          case DocumentChangeType.added:
            if (isDocExists.isEmpty) {
              bool isActive;
              try {
                isActive = docs.doc.get('isActive');
              } catch (e) {
                isActive = false;
              }
              if (isActive) {
                final product = ProductCategoriesEntity();
                product.catImg = docs.doc.get('productCatImg');
                // product.catGujaratiName = (await translator.translate(
                //         docs.doc.get('productCatName').toString(),
                //         to: 'gu'))
                //     .text;
                // product.catHindiName = (await translator.translate(
                //         docs.doc.get('productCatName').toString(),
                //         to: 'hi'))
                //     .text;
                product.catName = docs.doc.get('productCatName');
                product.catID = docs.doc.id;
                product.isActive = isActive;
                box.put(product, mode: PutMode.insert);
              }
            }
            break;
          case DocumentChangeType.modified:
            if (isDocExists.isNotEmpty) {
              var productCat = box.get(isDocExists.first.id)!;
              // productCat?.catGujaratiName = (await translator.translate(
              //         docs.doc.get('productCatName').toString(),
              //         to: 'gu'))
              //     .text;
              // productCat?.catHindiName = (await translator.translate(
              //         docs.doc.get('productCatName').toString(),
              //         to: 'hi'))
              //     .text;
              productCat.catName = docs.doc.get('productCatName');
              productCat.catImg = docs.doc.get('productCatImg');
              productCat.isActive = docs.doc.get('isActive');
              box.put(productCat, mode: PutMode.update);
            }
            break;
          case DocumentChangeType.removed:
            if (isDocExists.isNotEmpty) {
              box.remove(isDocExists.first.id);
            }
            break;
        }
      }
    });
  }

  listenProductsListChanges(String catID) {
    var listenChangesProductsList = FirestoreServices()
        .productCategoriesCollRef
        .doc(catID)
        .collection('productsList')
        .snapshots();
    Box<ProductCategoriesEntity> box =
        objectboxStore.box<ProductCategoriesEntity>();
    Box<ProductsListEntity> productsLstBox =
        objectboxStore.box<ProductsListEntity>();
    listenChangesProductsList.forEach((col) async {
      for (var docs in col.docChanges) {
        var document = docs.doc;
        List<ProductsListEntity> isDocExists;

        var currentCat =
            box.getAll().firstWhere((element) => element.catID == catID);
        isDocExists = currentCat.products
            .where((element) => element.productID == document.id)
            .toList();
        switch (docs.type) {
          case DocumentChangeType.added:
            // print('ADD and IN IF');
            if (isDocExists.isEmpty) {
              // create a new product with a product category=
              final product = ProductsListEntity();
              product.productCat.target = currentCat;
              product.productID = document.id;
              product.productImg = document.get('productImg');
              product.productName = document.get('productName');
              currentCat.products.add(product);
              box.put(currentCat);
              productsLstBox.put(product);
            }
            break;
          case DocumentChangeType.modified:
            if (isDocExists.isNotEmpty) {
              // print('modified and IN IF');
              var product = currentCat.products
                  .where((element) => element.productID == document.id)
                  .first;
              product.productImg = document.get('productImg');
              product.productName = document.get('productName');
              productsLstBox.put(product);
              // box.put(currentCat);
            }
            break;
          case DocumentChangeType.removed:
            if (isDocExists.isNotEmpty) {
              // print('removed and IN IF');
              box.remove(isDocExists.first.id);
            }
            break;
        }
      }
    });
  }

  listenProductDetailsChanges(String catID, String productID) async {
    var listenChangesProductDetailsList = FirestoreServices()
        .productCategoriesCollRef
        .doc(catID)
        .collection('productsList')
        .doc(productID)
        .collection('productDetails')
        .snapshots();
    Box<ProductsListEntity> box = objectboxStore.box<ProductsListEntity>();
    listenChangesProductDetailsList.forEach((col) {
      for (var docs in col.docChanges) {
        var documentData = docs.doc.data()!;
        var currentProduct =
            box.getAll().where((element) => element.productID == productID);

        switch (docs.type) {
          case DocumentChangeType.added:
            if (currentProduct.first.productDetails.target == null) {
              debugPrint('IF ADDED');
              final productDetailsEntity = ProductDetailsEntity();
              productDetailsEntity.productPrice = documentData['productPrice'];
              productDetailsEntity.productCompanyName =
                  documentData['productCompanyName'];
              productDetailsEntity.productWeight =
                  documentData['productWeight'];
              productDetailsEntity.product.target = currentProduct.first;
              currentProduct.first.productDetails.target = productDetailsEntity;
              debugPrint('currentProduct : $currentProduct');
              debugPrint('currentProduct.first : ${currentProduct.first}');
              box.put(currentProduct.first);
            }
            break;
          case DocumentChangeType.modified:
            if (currentProduct.first.productDetails.target != null) {
              debugPrint('IF modified');
              var product = box.get(currentProduct.first.id)!;
              product.productDetails.target!.productPrice =
                  documentData['productPrice'];
              product.productDetails.target!.productCompanyName =
                  documentData['productCompanyName'];
              product.productDetails.target!.productWeight =
                  documentData['productWeight'];
              // product.productDetails.target!.qtyInCart =
              //     documentData['qtyInCart'];
              box.put(product, mode: PutMode.update);
            }
            break;
          case DocumentChangeType.removed:
            debugPrint('IF removed');
            if (currentProduct.first.productDetails.target != null) {
              box.remove(currentProduct.first.id);
            }
            break;
        }
      }
    });
  }

  Future<DocumentSnapshot<Object?>> getProductByProductId(
      String catID, String productID) async {
    return await FirestoreServices()
        .productCategoriesCollRef
        .doc(catID)
        .collection('productsList')
        .doc(productID)
        .get();
  }

  getAndStoreProductDetails(String catID, String docID,
      [int qtyInCart = 0]) async {
    // [int? boxID]
    Box<ProductsListEntity> box = objectboxStore.box<ProductsListEntity>();
    Box<ProductDetailsEntity> productDetailEntityBox =
    objectboxStore.box<ProductDetailsEntity>();
    var productDetailsList = await FirestoreServices()
        .productCategoriesCollRef
        .doc(catID)
        .collection('productsList')
        .doc(docID)
        .collection('productDetails')
        .get();
    if (productDetailsList.docChanges.isNotEmpty &&
        productDetailsList.docChanges.first.doc.exists) {
      var productDetails = productDetailsList.docChanges.first.doc.data()!;
      var currentProduct =
      box.getAll().where((element) => element.productID == docID);
      if (currentProduct.isNotEmpty) {
        final productDetailEntity = currentProduct.first.productDetails
            .target == null ?
        ProductDetailsEntity() : currentProduct.first.productDetails.target!;
        productDetailEntity.productCompanyName =
        productDetails['productCompanyName'];
        productDetailEntity.productPrice = productDetails['productPrice'];
        productDetailEntity.productWeight = productDetails['productWeight'];
        productDetailEntity.qtyInCart = qtyInCart;
        productDetailEntity.isInCart = qtyInCart > 0 ? true : false;
        productDetailEntity.product.target = currentProduct.first;
        currentProduct.first.productDetails.target = productDetailEntity;
        box.put(currentProduct.first);
        productDetailEntityBox.put(productDetailEntity);
      }
      // else {
      //   ProductDetailsEntity currentProductDetails =
      //       currentProduct.first.productDetails.target!;
      //   currentProductDetails.product.target = currentProduct.first;
      //   currentProductDetails.productCompanyName =
      //       productDetails['productCompanyName'];
      //   currentProductDetails.productPrice = productDetails['productPrice'];
      //   currentProductDetails.productWeight = productDetails['productWeight'];
      //   currentProductDetails.qtyInCart=qtyInCart;
      //   currentProductDetails.isInCart=qtyInCart>0?true:false;
      //   currentProduct.first.productDetails.target = currentProductDetails;
      //   box.put(currentProduct.first);
      //   productDetailEntityBox.put(currentProductDetails);
      // }
    }
  }

  addProductListIfNotThere(String productID, String catID) async {
    // [int? iD]
    Box<ProductCategoriesEntity> productCatBox =
        objectboxStore.box<ProductCategoriesEntity>();
    Box<ProductsListEntity> productListBox =
        objectboxStore.box<ProductsListEntity>();
    var isProductExistsInList = productListBox
        .getAll()
        .where((element) => element.productID == productID);

    if (isProductExistsInList.isEmpty) {
      var productNameImg = await getProductByProductId(catID, productID);
      try {
        var currentCat = objectboxStore
            .box<ProductCategoriesEntity>()
            .getAll()
            .where((element) => element.catID == catID);
        final product = ProductsListEntity();
        product.productID = productID;
        product.productCat.target = currentCat.first;
        product.productName = productNameImg.get('productName');
        product.productImg = productNameImg.get('productImg');
        currentCat.first.products.add(product);
        productCatBox.put(currentCat.first);
        productListBox.put(product);
      } catch (e) {
        if (kDebugMode) {
          print('E : ${e.toString()}');
          print('Method : addProductListIfNotThere : CATCH BLOCK!');
        }
      }
    }
    await getAndStoreProductDetails(catID, productID);
  }

  listenCartListChanges() async {
    if (AuthServices().currentUser != null) {
      var updatedCartList = FirestoreServices()
          .allUsersCollRef
          .doc(AuthServices().currentUser!.uid)
          .collection('userCartList')
          .snapshots();
      Box<ProductsListEntity> box = objectboxStore.box<ProductsListEntity>();
      Box<ProductDetailsEntity> productDetailsEntityBox =
          objectboxStore.box<ProductDetailsEntity>();
      updatedCartList.forEach((col) async {
        for (var docs in col.docChanges) {
          var documentData = docs.doc.data()!;
          Iterable<ProductsListEntity> isDocExists = [];
          isDocExists =
              box.getAll().where((element) => element.productID == docs.doc.id);
          switch (docs.type) {
            case DocumentChangeType.added:
              if (isDocExists.isEmpty) {
                await addProductListIfNotThere(
                    docs.doc.id, documentData['catID']);
              }
              isDocExists = box
                  .getAll()
                  .where((element) => element.productID == docs.doc.id);
              if (isDocExists.isNotEmpty &&
                  isDocExists.first.productDetails.target != null) {
                ProductsListEntity productListEntity = isDocExists.first;
                ProductDetailsEntity productDetailsEntity =
                    isDocExists.first.productDetails.target!;
                productListEntity.productDetails.target!.isInCart = true;
                productListEntity.productDetails.target!.qtyInCart =
                    documentData['qtyInCart'];
                productDetailsEntity.isInCart = true;
                productDetailsEntity.qtyInCart = documentData['qtyInCart'];
                productDetailsEntityBox.put(productDetailsEntity);
                box.put(productListEntity);
              }
              break;
            case DocumentChangeType.modified:
              if (isDocExists.isNotEmpty) {
                await getAndStoreProductDetails(
                    documentData['catID'], docs.doc.id,
                    documentData['qtyInCart']);
              }
              break;
            case DocumentChangeType.removed:
              if (isDocExists.isNotEmpty) {
                ProductDetailsEntity pDetails =
                    isDocExists.first.productDetails.target!;
                pDetails.qtyInCart = 0;
                pDetails.isInCart = false;
                productDetailsEntityBox.put(pDetails);
                box.put(isDocExists.first);
              }
              break;
          }
        }
      });
    }
  }

  addProductToCart(String catID, String productID) async {
    try {
      var timeStamp = FieldValue.serverTimestamp();
      await FirestoreServices()
          .allUsersCollRef
          .doc(AuthServices().currentUser!.uid)
          .collection('userCartList')
          .doc(productID)
          .set({
        'catID': catID,
        'qtyInCart': 1,
        'addedInCartTime': timeStamp,
      });
      Utilities().showSnackBar('Added to the cart list');

      // Box<ProductsListEntity> box = objectboxStore.box<ProductsListEntity>();
      // Box<ProductDetailsEntity> boxProductDetails =
      //     objectboxStore.box<ProductDetailsEntity>();
      // Iterable<ProductsListEntity> currentProduct = [];
      // currentProduct =
      //     box.getAll().where((element) => element.productID == productID);
      // var currentProductDetail = boxProductDetails
      //     .getAll()
      //     .where((element) =>
      // element.product.target!.productID ==
      //     currentProduct.first.productID)
      //     .first;
      // currentProductDetail.isInCart = true;
      // currentProductDetail.qtyInCart = 1;
      // currentProductDetail.addedInCartTime = timeStamp.toString();
      // currentProductDetail.product.target = currentProduct.first;
      // box.put(currentProduct.first, mode: PutMode.update);
      // boxProductDetails.put(currentProductDetail, mode: PutMode.update);
    } catch (e) {
      debugPrint('E : $e');
      Utilities().showSnackBar('Something wrong happened can\'t add to cart');
    }
  }

  addOrRemoveProductToWishList(
    bool isInWishList,
    String catID,
    String productID,
    Box<ProductsListEntity> box,
  ) async {
    switch (isInWishList) {
      case true:
        await FirestoreServices()
            .allUsersCollRef
            .doc(AuthServices().currentUser!.uid)
            .collection('userWishList')
            .doc(productID)
            .delete();
        Utilities().showSnackBar('Removed from the wish list');
        break;
      case false:
        await FirestoreServices()
            .allUsersCollRef
            .doc(AuthServices().currentUser!.uid)
            .collection('userWishList')
            .doc(productID)
            .set({
          'catID': catID,
        });
        Utilities().showSnackBar('Added in the wish list');
        break;
    }
  }

  removeProductFromCart(String catID, String productID, int qtyInCart,
      [int index = 0, AnimatedListState? currentState]) async {
    try {
      await FirestoreServices()
          .allUsersCollRef
          .doc(AuthServices().currentUser!.uid)
          .collection('userCartList')
          .doc(productID)
          .delete();
      await FirestoreServices()
          .allUsersCollRef
          .doc(AuthServices().currentUser!.uid)
          .update({'userCart': FieldValue.increment(-qtyInCart)});
      print('removeProductFromCart : QTY : $qtyInCart');
      print('removeProductFromCart : QTY : ${-qtyInCart}');
      Utilities().showSnackBar('Removed to the cart list');
      // Box<ProductsListEntity> box = objectboxStore.box<ProductsListEntity>();
      // Box<ProductDetailsEntity> boxProductDetails =
      //     objectboxStore.box<ProductDetailsEntity>();
      // Iterable<ProductsListEntity> currentProduct = [];
      // currentProduct =
      //     box.getAll().where((element) => element.productID == productID);
      // ProductDetailsEntity currentProductDetail = boxProductDetails
      //     .getAll()
      //     .where((element) =>
      //         element.product.target!.productID ==
      //         currentProduct.first.productID)
      //     .first;
      // currentProductDetail.isInCart = false;
      // currentProductDetail.qtyInCart = 0;
      // currentProductDetail.product.target = currentProduct.first;
      // currentProduct.first.productDetails.target!.isInCart = false;
      // currentProduct.first.productDetails.target!.qtyInCart = 0;
      // box.put(currentProduct.first, mode: PutMode.update);
      // boxProductDetails.put(currentProductDetail, mode: PutMode.update);
    } catch (e) {
      Utilities()
          .showSnackBar('Something wrong happened can\'t remove from cart');
    }
  }

  listenOrdersListChanges() {
    if (AuthServices().currentUser != null) {
      var updatedOrdersList = FirestoreServices()
          .getOrdersCollRef(AuthServices().currentUser!.uid)
          .snapshots();
      Box<OrderEntity> box = objectboxStore.box<OrderEntity>();
      updatedOrdersList.forEach((col) async {
        for (var docs in col.docChanges) {
          Map<String, dynamic> documentData =
              docs.doc.data()! as Map<String, dynamic>;
          Iterable<OrderEntity> isDocExists = [];

          isDocExists =
              box.getAll().where((element) => element.orderID == docs.doc.id);
          // Store order, can order same products multiple times...
          switch (docs.type) {
            case DocumentChangeType.added:
              if (isDocExists.isEmpty) {
                final newOrderEnt = OrderEntity();
                List<OrderItemEntity> orderedItemsList = [];
                for (var order in documentData['orderList']) {
                  await addProductListIfNotThere(
                      order['productID'], order['catID']);
                }

                for (var order in documentData['orderList']) {
                  OrderItemEntity item = OrderItemEntity();
                  item.catID = order['catID'];
                  item.productID = order['productID'];
                  item.qtyForOrder = order['qtyForOrder'];
                  orderedItemsList.add(item);
                }
                newOrderEnt.items.addAll(orderedItemsList);
                newOrderEnt.orderID = docs.doc.id;
                newOrderEnt.orderedTime =
                    documentData['orderedTime'].toString();
                newOrderEnt.orderedToAddress = documentData['orderedToAddress'];
                newOrderEnt.orderedTotalPrice =
                    documentData['orderedTotalPrice'];
                box.put(newOrderEnt);
              }
              break;
            case DocumentChangeType.modified:
            // We can modify order delivery address...
              if (isDocExists.isNotEmpty) {
                OrderEntity item = box.get(isDocExists.first.id)!;
                item.orderedToAddress = documentData['orderedToAddress'];
                box.put(item);
              }
              break;
            case DocumentChangeType.removed:
            // We can't remove the order
              if (kDebugMode) {
                box.remove(isDocExists.first.id);
              }
              break;
          }
        }
      });
    }
  }

  List<ProductsListEntity> getCurrentProductsObjBox(productID) {
    Box<ProductsListEntity> productBox =
        objectboxStore.box<ProductsListEntity>();
    return productBox
        .getAll()
        .where((product) => product.productID == productID)
        .toList();
  }

  List<ProductDetailsEntity> getCurrentProductDetailsObjBox(productID) {
    Box<ProductDetailsEntity> productDetailsBox =
        objectboxStore.box<ProductDetailsEntity>();
    return productDetailsBox
        .getAll()
        .where((element) => element.product.target!.productID == productID)
        .toList();
  }

  listenWishListChanges() async {
    if (AuthServices().currentUser != null) {
      var updatedWishList = FirestoreServices()
          .allUsersCollRef
          .doc(AuthServices().currentUser!.uid)
          .collection('userWishList')
          .snapshots();
      Box<ProductsListEntity> productBox =
          objectboxStore.box<ProductsListEntity>();
      Box<ProductDetailsEntity> productDetailsBox =
          objectboxStore.box<ProductDetailsEntity>();
      updatedWishList.forEach((col) async {
        for (var document in col.docChanges) {
          var product = getCurrentProductsObjBox(document.doc.id);
          switch (document.type) {
            case DocumentChangeType.added:
              Map<String, dynamic> docData =
                  document.doc.data() as Map<String, dynamic>;
              if (product.isEmpty ||
                  product.first.productDetails.target == null) {
                await addProductListIfNotThere(
                    document.doc.id, docData['catID']);
              }
              var productDetails =
                  getCurrentProductDetailsObjBox(document.doc.id).first;
              productDetails.isInWish = true;
              productDetailsBox.put(productDetails);
              break;
            case DocumentChangeType.modified:
            // Wish list can't modify
              break;
            case DocumentChangeType.removed:
              if (product.isNotEmpty) {
                var productRemovedFromWishList =
                    product.first.productDetails.target!;
                productRemovedFromWishList.isInWish = false;
                product.first.productDetails.target!.isInWish = false;
                productDetailsBox.put(productRemovedFromWishList);
                productBox.put(product.first);
              }
              break;
          }
        }
      });
    }
  }
}
