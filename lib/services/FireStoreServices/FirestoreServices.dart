import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:my_mart/main.dart';

class FirestoreServices {
  CollectionReference allUsersCollRef = fireStore.collection('allUsers');
  CollectionReference productCategoriesCollRef =
      fireStore.collection('productCategories');
  static String ordersCollName = 'userOrderList';

  CollectionReference<Map<String, dynamic>> getProductDetailsColRef(
      String catId, String productId) {
    return productCategoriesCollRef
        .doc(catId)
        .collection('productsList')
        .doc(productId)
        .collection('productDetails');
  }

  CollectionReference<Map<String, dynamic>> getProductSellerCollRef(
      String catId, String productId) {
    return productCategoriesCollRef
        .doc(catId)
        .collection('productsList')
        .doc(productId)
        .collection('productSellers');
  }

  CollectionReference getOrdersCollRef(String userUID) {
    return allUsersCollRef.doc(userUID).collection(ordersCollName);
  }
}
