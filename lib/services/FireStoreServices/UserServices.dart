import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/foundation.dart';
import 'package:my_mart/services/FireStoreServices/FirestoreServices.dart';
import 'package:translator/translator.dart';

import '../../main.dart';
import '../../objectbox.g.dart';
import '../../utility/ObjectBoxEntities/AppData/EntityAppData.dart';
import '../../utility/ObjectBoxEntities/User/UserEntity.dart';
import '../../utility/Utilities.dart';
import '../AuthServices.dart';

class UserServices {
  addUser(String userName, String userEmail, String userId,
      [bool userEmailVerified = false]) async {
    // Call the user's CollectionReference to add a new user
    try {
      final fcmToken = await Utilities().getFirebaseMessagingToken();
      var userData = {
        'userCreationTime': Timestamp.now(),
        'userLastSignInTime': Timestamp.now(),
        'userAddress': '',
        'userCart': 0,
        'userCity': '',
        'userCompletedOrders': 0,
        'userEmail': userEmail,
        'userEmailVerified': userEmailVerified,
        'userGender': '',
        'userName': userName,
        'userPendingOrders': 0,
        'userPhone': '',
        'userPhoneVerified': false,
        'userState': '',
        'userWish': 0,
        'firebaseMessagingToken': fcmToken
      };
      await FirestoreServices().allUsersCollRef.doc(userId).set(userData);
      final storeUserData = UserEntity(
          userUID: userId,
          userCreationTime: AuthServices().currentUser?.metadata.creationTime ??
              DateTime.now(),
          userLastSignInTime:
              AuthServices().currentUser?.metadata.lastSignInTime ??
                  DateTime.now(),
          userAddress: '',
          userCart: 0,
          userCity: '',
          userCompletedOrders: 0,
          userEmail: userEmail,
          userEmailVerified: false,
          userGender: '',
          userName: userName,
          userPendingOrders: 0,
          userPhone: '',
          userPhoneVerified: false,
          userState: '',
          userWish: 0,
          firebaseMessagingToken: fcmToken);
      objectboxStore.box<UserEntity>().put(storeUserData, mode: PutMode.insert);
    } catch (e) {
      debugPrint(e.toString());
    }
  }

  DocumentReference<Object?> getCurrentUserRef(String userUID) {
    return FirestoreServices().allUsersCollRef.doc(userUID);
  }

  Future<DocumentSnapshot> getCurrentUserDoc(String userUID) {
    return getCurrentUserRef(userUID).get();
  }

  updateCurrentUser(String userNameChange) async {
    if (AuthServices().currentUser?.displayName != userNameChange &&
        userNameChange.length > 3) {
      await AuthServices().currentUser?.updateDisplayName(userNameChange);
      await FirestoreServices()
          .allUsersCollRef
          .doc(AuthServices().currentUser?.uid)
          .update({'userName': userNameChange});
      Box<UserEntity> box = objectboxStore.box<UserEntity>();
      var currentUserDetails =
          box.get(objectboxStore.box<UserEntity>().getAll().last.id);
      currentUserDetails!.userName = userNameChange;
      box.put(currentUserDetails, mode: PutMode.update);
      Utilities().showSnackBar('Display Name update success');
    }
  }

  updateCurrentUserPhone(userPhone, credential) async {
    await AuthServices().currentUser!.updatePhoneNumber(credential);
    Box<UserEntity> box = objectboxStore.box<UserEntity>();
    var currentUserDetails =
        box.get(objectboxStore.box<UserEntity>().getAll().last.id);
    currentUserDetails!.userPhone = userPhone;
    currentUserDetails.userPhoneVerified = true;
    box.put(currentUserDetails, mode: PutMode.update);
    await UserServices()
        .getCurrentUserRef(AuthServices().currentUser!.uid)
        .update({'userPhone': userPhone, 'userPhoneVerified': true});
    Utilities().showSnackBar('Phone number is updated!');
  }

  listenUserDataChange() async {
    getCurrentUserRef(AuthServices().currentUser!.uid).snapshots().listen(
          (event) => updateUserLocallyAfterListenChanges(event.data() as Map),
          onError: (error) => print("Listen failed: $error"),
        );
  }

  updateUserLocallyAfterListenChanges(Map userData) async {
    try {
      var appData = objectboxStore.box<EntityAppData>().getAll().first;
      var userBox = objectboxStore.box<UserEntity>();
      var localUser = userBox
          .query(UserEntity_.userUID.equals(AuthServices().currentUser!.uid))
          .build()
          .find()
          .first;
      if (appData.userCurrentLanguage != 'en') {
        try {
          var res = await GoogleTranslator().translate(
              localUser.userName.toString(),
              to: appData.userCurrentLanguage.toString());
          localUser.userName = res.text;
        } catch (e) {
          debugPrint('E User is not there or CATCH : $e');
        }
      } else {
        localUser.userName = userData['userName'];
      }
      localUser.userAddress = userData['userAddress'];
      localUser.userCity = userData['userCity'];
      localUser.userCompletedOrders = userData['userCompletedOrders'];
      localUser.userCart = userData['userCart'];
      localUser.userEmailVerified = userData['userEmailVerified'];
      localUser.userPhoneVerified = userData['userPhoneVerified'];
      localUser.userPhone = userData['userPhone'];
      localUser.userLastSignInTime = userData['userLastSignInTime'].toDate();
      userBox.put(localUser);
    } catch (e) {
      if (kDebugMode) {
        print(
            "Can't update user local data : Method Name - updateUserLocallyAfterListenChanges");
        print(e);
      }
    }
  }
}
