import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:my_mart/services/FireStoreServices/UserServices.dart';
import 'package:my_mart/utility/Utilities.dart';

import '../main.dart';
import '../utility/ObjectBoxEntities/User/UserEntity.dart';

class AuthServices {
  var currentUser = auth.currentUser;

  Future<User> handleSignInEmail(
      [User? user, String email = '', String password = '']) async {
    try {
      user ??= (await auth.signInWithEmailAndPassword(
              email: email, password: password))
          .user!;
      await firebaseAnalytics.setUserId(id: user.uid);
      await firebaseAnalytics.logLogin(
          loginMethod: password.length > 3 ? 'email' : 'google');
      final fcmToken = await Utilities().getFirebaseMessagingToken();
      await UserServices().getCurrentUserRef(user.uid).update({
        'userLastSignInTime': Timestamp.fromDate(user.metadata.lastSignInTime!),
        'firebaseMessagingToken': fcmToken
      });
      Map<String, dynamic> userData =
          (await UserServices().getCurrentUserDoc(user.uid)).data()
              as Map<String, dynamic>;
      objectboxStore.box<UserEntity>().put(UserEntity(
          userAddress: userData['userAddress'],
          userCart: userData['userCart'],
          userCity: userData['userCity'],
          userCompletedOrders: userData['userCompletedOrders'] ?? 0,
          userCreationTime: userData['userCreationTime'].toDate(),
          userEmail: userData['userEmail'],
          userEmailVerified: userData['userEmailVerified'],
          userGender: userData['userGender'],
          userLastSignInTime: userData['userLastSignInTime'].toDate(),
          userName: userData['userName'],
          userPendingOrders: userData['userPendingOrders'],
          userPhone: userData['userPhone'],
          userPhoneVerified: userData['userPhoneVerified'],
          userState: userData['userState'],
          userUID: user.uid,
          userWish: userData['userWish'],
          firebaseMessagingToken: fcmToken));
    } catch (e) {
      print('e: $e');
      throw '$e';
    }
    return user;
  }

  logOutUser() async {
    try {
      await auth.signOut();
      Utilities().deleteWholeDataFromLocal();
      await GoogleSignIn().signOut();
      await GoogleSignIn().disconnect();
      Utilities().showSnackBar('Logout Success');
    } catch (e) {
      print(e);
    }
  }

  handleSignUp(
      [User? user,
      String userName = '',
      String userEmail = '',
      String userPassword = '']) async {
    user ??= (await auth.createUserWithEmailAndPassword(
            email: userEmail, password: userPassword))
        .user!;
    await firebaseAnalytics.setUserId(id: user.uid);
    await firebaseAnalytics.logSignUp(
        signUpMethod: userPassword.length > 3 ? 'email' : 'google');
    await user.sendEmailVerification();

    return await UserServices().addUser(
        userName.isNotEmpty ? userName : user.displayName.toString(),
        userEmail.isNotEmpty ? userEmail.trim() : user.email.toString(),
        user.uid,
        userPassword.length < 3 ? true : false);
  }

  Future forgetPass(String userEmail) {
    return auth.sendPasswordResetEmail(email: userEmail);
  }
}
