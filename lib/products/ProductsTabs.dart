import 'package:flutter/material.dart';
import 'package:get/get_utils/src/extensions/internacionalization.dart';
import 'package:my_mart/products/ProductsOrderTabBarViews/TabBarViewCartList.dart';
import 'package:my_mart/products/ProductsOrderTabBarViews/TabBarViewOrdersList.dart';
import 'package:my_mart/products/ProductsOrderTabBarViews/TabBarViewWishList.dart';

class ProductsOrder extends StatefulWidget {
  const ProductsOrder({Key? key}) : super(key: key);

  @override
  State<ProductsOrder> createState() => _ProductsOrderState();
}

class _ProductsOrderState extends State<ProductsOrder> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        length: 3,
        child: Scaffold(
          appBar: PreferredSize(
            preferredSize: const Size.fromHeight(kToolbarHeight),
            child: ColoredBox(
              color: Colors.green,
              child: TabBar(
                  unselectedLabelColor: Colors.lightGreenAccent,
                  indicatorColor: Colors.green,
                  labelColor: Colors.white,
                  tabs: [
                    Tab(text: 'Cart List'.tr),
                    Tab(text: 'Wish List'.tr),
                    Tab(text: 'Orders List'.tr),
                  ]),
            ),
          ),
          body: const TabBarView(
            children: [
              TabBarViewCartList(),
              TabBarViewWishList(),
              TabBarViewOrdersList(),
            ],
          ),
        ));
  }
}
