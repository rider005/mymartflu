import 'dart:async';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart' as firestore;
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:lottie/lottie.dart';
import 'package:my_mart/objectbox.g.dart';
import 'package:my_mart/otherFiles/Screens/ShowLoadingScreen.dart';
import 'package:my_mart/utility/Utilities.dart';

import '../main.dart';
import '../services/AuthServices.dart';
import '../services/FireStoreServices/FirestoreServices.dart';
import '../services/FireStoreServices/ProductServices.dart';
import '../utility/ObjectBoxEntities/ProductCat/EntityForProducts.dart';

class ProductDetailsScreen extends StatefulWidget {
  final String productId;
  final String catId;
  final Map<String, dynamic> productInfo;

  const ProductDetailsScreen({
    Key? key,
    required this.catId,
    required this.productId,
    required this.productInfo,
  }) : super(key: key);

  @override
  ProductDetailsScreenState createState() => ProductDetailsScreenState();
}

class ProductDetailsScreenState extends State<ProductDetailsScreen>
    with SingleTickerProviderStateMixin {
  late Stream<List<ProductsListEntity>> _productDetailsStream;
  Box<ProductsListEntity> box = objectboxStore.box<ProductsListEntity>();
  RxBool showAnim = false.obs, isInWishList = false.obs;
  late AnimationController _controller;
  RxString animationJsonLink = ''.obs;
  late double forTitlePartWidth, forValuePartWidth;

  @override
  Widget build(BuildContext context) {
    forTitlePartWidth = MediaQuery.of(context).size.width * 0.30;
    forValuePartWidth = MediaQuery.of(context).size.width * 0.40;
    return Scaffold(
      appBar: AppBar(
        title: const Text('Products Details'),
        actions: [getActions()],
        leading: const BackButton(color: Colors.white),
      ),
      body: StreamBuilder(
        stream: _productDetailsStream,
        builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
          if (!snapshot.hasError &&
              snapshot.hasData &&
              snapshot.connectionState == ConnectionState.active &&
              (snapshot.data as List<ProductsListEntity>).isNotEmpty) {
            var productsList =
                (snapshot.data as List<ProductsListEntity>).first;
            if (productsList.productDetails.target != null) {
              Timer(Duration.zero, () {
                isInWishList.value =
                    productsList.productDetails.target!.isInWish ?? false;
              });
              return Obx(() =>
              showAnim.isTrue
                  ? Center(
                child: Lottie.network(animationJsonLink.value,
                    controller: _controller,
                    frameRate: FrameRate.max,
                    repeat: true, onLoaded: (composition) {
                      _controller.duration = composition.duration;
                      _controller.forward();
                    }),
              )
                  : Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Expanded(
                    child: Utilities().clipImg(CachedNetworkImage(
                      placeholder: (context, url) => const Center(
                          child: ShowLoadingScreen(color: Colors.white)),
                      imageUrl: widget.productInfo['productImg'],
                    )),
                  ),
                  Expanded(
                      flex: 3,
                      child: Column(
                        children: [
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              FloatingActionButton.extended(
                                onPressed: () => _addOrRemoveProductCart(
                                    productsList.productDetails.target!
                                        .isInCart ==
                                        true
                                        ? true
                                        : false,
                                    0),
                                tooltip:
                                'Click here to add product to cart list',
                                label: productsList.productDetails.target!
                                    .isInCart !=
                                    true ||
                                    productsList.productDetails
                                        .target!.isInCart ==
                                        null
                                    ? const Text('Add to cart')
                                    : const Text('Remove from cart'),
                                icon: Icon(productsList.productDetails
                                    .target!.isInCart !=
                                    true
                                    ? Icons.add_shopping_cart_outlined
                                    : Icons
                                    .remove_shopping_cart_outlined),
                                backgroundColor: Colors.green,
                              ),
                            ],
                          ),
                          Row(
                            mainAxisAlignment:
                            MainAxisAlignment.spaceAround,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              SizedBox(
                                  width: forTitlePartWidth,
                                  child: const Text(
                                    'Category Name',
                                    maxLines: 1,
                                    overflow: TextOverflow.visible,
                                    softWrap: false,
                                  )),
                              SizedBox(
                                  width: forValuePartWidth,
                                  child: Text(
                                    '${widget.productInfo['catName']}',
                                    maxLines: 1,
                                    overflow: TextOverflow.visible,
                                    softWrap: false,
                                  )),
                            ],
                          ),
                          Row(
                            mainAxisAlignment:
                            MainAxisAlignment.spaceAround,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              SizedBox(
                                  width: forTitlePartWidth,
                                  child: const Text(
                                    'Company Name',
                                    maxLines: 1,
                                    overflow: TextOverflow.visible,
                                    softWrap: false,
                                  )),
                              SizedBox(
                                  width: forValuePartWidth,
                                  child: Text(
                                      '${productsList.productDetails.target!.productCompanyName!.isNotEmpty ? productsList.productDetails.target!.productCompanyName : 'NA'}')),
                            ],
                          ),
                          Row(
                            mainAxisAlignment:
                            MainAxisAlignment.spaceAround,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              SizedBox(
                                  width: forTitlePartWidth,
                                  child: const Text('Product Name')),
                              SizedBox(
                                  width: forValuePartWidth,
                                  child: Text(
                                    '${widget.productInfo['productName']}',
                                    maxLines: 3,
                                    overflow: TextOverflow.ellipsis,
                                    softWrap: false,
                                  )),
                            ],
                          ),
                          Row(
                            mainAxisAlignment:
                            MainAxisAlignment.spaceAround,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              SizedBox(
                                  width: forTitlePartWidth,
                                  child: const Text(
                                    'Product Weight',
                                    maxLines: 1,
                                    overflow: TextOverflow.visible,
                                    softWrap: false,
                                  )),
                              SizedBox(
                                  width: forValuePartWidth,
                                  child: Text(
                                    '${productsList.productDetails.target!.productWeight}',
                                  )),
                            ],
                          ),
                          Row(
                            mainAxisAlignment:
                            MainAxisAlignment.spaceAround,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              SizedBox(
                                  width: forTitlePartWidth,
                                  child: const Text('Product Price')),
                              SizedBox(
                                  width: forValuePartWidth,
                                  child: Text(
                                      '${productsList.productDetails.target!.productPrice} Rs.')),
                            ],
                          ),
                        ]
                            .map((child) => Column(
                          mainAxisAlignment:
                          MainAxisAlignment.center,
                          crossAxisAlignment:
                          CrossAxisAlignment.center,
                          children: [
                            Padding(
                              padding: const EdgeInsets.symmetric(
                                  vertical: 3),
                              child: child,
                            ),
                            const Divider(
                              color: Colors.green,
                              thickness: 3,
                            )
                          ],
                        ))
                            .toList(),
                      )),
                ],
              ));
            }
          }
          ProductServices()
              .listenProductDetailsChanges(widget.catId, widget.productId);
          return const ShowLoadingScreen(color: Colors.white);
        },
      ),
    );
  }

  Widget getActions() {
    return Obx(() => IconButton(
      // Use the FaIcon Widget + FontAwesomeIcons class for the IconData
        icon: FaIcon(
            isInWishList.value
                ? FontAwesomeIcons.heartCircleCheck
                : FontAwesomeIcons.heartCirclePlus,
            color: Colors.white),
        onPressed: () {
          _addOrRemoveProductFromWishList(isInWishList.value);
        }));
  }

  _addOrRemoveProductCart(bool isInCart, qtyInCart) async {
    debugPrint('isInCart : $isInCart');
    switch (isInCart) {
      case true:
        animationJsonLink.value =
        'https://assets9.lottiefiles.com/packages/lf20_8ykarsfv.json';
        showAnim.value = true;
        await ProductServices()
            .removeProductFromCart(widget.catId, widget.productId, qtyInCart);
        break;
      case false:
        if (widget.catId.isNotEmpty) {
          animationJsonLink.value =
          'https://assets1.lottiefiles.com/packages/lf20_gsigmrhp.json';
          showAnim.value = true;
          await ProductServices()
              .addProductToCart(widget.catId, widget.productId);
          await FirestoreServices()
              .allUsersCollRef
              .doc(AuthServices().currentUser!.uid)
              .update({'userCart': firestore.FieldValue.increment(1)});
          return;
        }
        break;
    }
  }

  _addOrRemoveProductFromWishList(bool isInWish) async {
    await ProductServices().addOrRemoveProductToWishList(
        isInWish, widget.catId, widget.productId, box);
    isInWishList.value = isInWish != true ? true : false;
    if (!isInWish) {
      animationJsonLink.value =
      'https://assets9.lottiefiles.com/packages/lf20_fltfkvb5.json';
      showAnim.value = true;
    }
  }

  @override
  void initState() {
    Utilities().startScreenTracking('Products Details');
    QueryBuilder<ProductsListEntity> builder = objectboxStore
        .box<ProductsListEntity>()
        .query(ProductsListEntity_.productID.equals(widget.productId));
    builder.backlink(ProductDetailsEntity_.product);
    _productDetailsStream =
        builder.watch(triggerImmediately: true).map((e) => e.find());

    _controller =
        AnimationController(vsync: this, duration: const Duration(seconds: 2));
    _controller.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        showAnim.value = false;
        _controller.reset();
      }
    });
    super.initState();
  }

  @override
  dispose() {
    _controller.dispose();
    super.dispose();
  }
}
