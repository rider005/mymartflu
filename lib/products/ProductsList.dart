import 'package:animations/animations.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get_utils/src/extensions/internacionalization.dart';
import 'package:my_mart/otherFiles/Screens/ShowLoadingScreen.dart';
import 'package:my_mart/products/ProductDetails.dart';
import 'package:my_mart/services/FireStoreServices/FirestoreServices.dart';
import 'package:my_mart/services/FireStoreServices/ProductServices.dart';
import 'package:my_mart/utility/Utilities.dart';

import '../main.dart';
import '../objectbox.g.dart';
import '../utility/ObjectBoxEntities/ProductCat/EntityForProducts.dart';

class ProductsListScreen extends StatefulWidget {
  final String catID;
  final String catName;

  const ProductsListScreen({Key? key, required this.catID, required this.catName})
      : super(key: key);

  @override
  ProductsListScreenState createState() => ProductsListScreenState();
}

class ProductsListScreenState extends State<ProductsListScreen> {
  var fireHandler = FirestoreServices();
  late Stream<List<ProductCategoriesEntity>> _productsListStream;

  @override
  void initState() {
    setState(() {
      QueryBuilder<ProductCategoriesEntity> builder = objectboxStore
          .box<ProductCategoriesEntity>()
          .query(ProductCategoriesEntity_.catID.equals(widget.catID));
      builder.backlink(ProductsListEntity_.productCat);
      _productsListStream =
          builder.watch(triggerImmediately: true).map((e) => e.find());
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Utilities().startScreenTracking('Products List');
    return Scaffold(
        appBar: AppBar(
          title: Text('productList'.tr),
          leading: const BackButton(
            color: Colors.white,
          ),
        ),
        body: StreamBuilder(
            stream: _productsListStream,
            builder: (ctx, snapshot) {
              if (!snapshot.hasError &&
                  snapshot.hasData &&
                  snapshot.connectionState == ConnectionState.active) {
                var productList =
                snapshot.data as List<ProductCategoriesEntity>;
                if (productList.isNotEmpty) {
                  var products = productList.first.products;
                  if (products.isNotEmpty) {
                    return GridView.builder(
                        gridDelegate:
                        const SliverGridDelegateWithFixedCrossAxisCount(
                            childAspectRatio: 1,
                            crossAxisCount: 2,
                            crossAxisSpacing: 10,
                            mainAxisSpacing: 10),
                        itemCount: products.length,
                        itemBuilder: (context, position) {
                          return Utilities().customContainer(OpenContainer(
                            closedColor: Colors.transparent,
                            middleColor: Colors.transparent,
                            openColor: Colors.transparent,
                            transitionDuration:
                                const Duration(milliseconds: 800),
                            transitionType: ContainerTransitionType.fade,
                            closedBuilder: (context, action) {
                              return GestureDetector(
                                onTap: action,
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Utilities().clipImg(CachedNetworkImage(
                                      height:
                                          MediaQuery.of(context).size.height /
                                              6,
                                      width: MediaQuery.of(context).size.width /
                                          2.5,
                                      placeholder: (context, url) =>
                                          const Center(
                                              child: ShowLoadingScreen(
                                                  color: Colors.white)),
                                      imageUrl: products[position]
                                          .productImg
                                          .toString(),
                                    )),
                                    Text(
                                      '${products[position].productName}'
                                          .toTitleCase(),
                                      style:
                                          const TextStyle(color: Colors.white),
                                      overflow: TextOverflow.ellipsis,
                                      softWrap: false,
                                      maxLines: 2,
                                      textAlign: TextAlign.center,
                                    ),
                                  ],
                                ),
                              );
                            },
                            openBuilder: (context, action) {
                              return ProductDetailsScreen(
                                  catId: widget.catID,
                                  productId:
                                      products[position].productID.toString(),
                                  productInfo: {
                                    'catName': widget.catName,
                                    'productName': products[position]
                                        .productName
                                        .toString(),
                                    'productImg':
                                        products[position].productImg.toString()
                                  });
                            },
                          ));
                        });
                  }
                }
                return const ShowLoadingScreen(color: Colors.white);
              } else {
                ProductServices().listenProductsListChanges(widget.catID);
                return const ShowLoadingScreen(
                  color: Colors.white,
                );
              }
            }));
  }
}
