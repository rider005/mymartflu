import 'package:flutter/material.dart';
import 'package:my_mart/main.dart';
import 'package:my_mart/products/OrderDetails.dart';

import '../../utility/ObjectBoxEntities/ProductCat/EntityForProducts.dart';

class TabBarViewOrdersList extends StatefulWidget {
  const TabBarViewOrdersList({Key? key}) : super(key: key);

  @override
  State<TabBarViewOrdersList> createState() => _TabBarViewOrdersListState();
}

class _TabBarViewOrdersListState extends State<TabBarViewOrdersList> {
  late Stream<List<OrderEntity>> _productsOrdersListStream;

  @override
  void initState() {
    setState(() {
      _productsOrdersListStream = objectboxStore
          .box<OrderEntity>()
          .query()
          .watch(triggerImmediately: true)
          .map((e) => e.find());
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    onOrderCardSelectedHandler(OrderEntity orderDetails) {
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (BuildContext context) =>
                  OrderDetailsScreen(mySelectedOrderDetails: orderDetails)));
    }

    return StreamBuilder(
        stream: _productsOrdersListStream,
        builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
          if (snapshot.hasData &&
              !snapshot.hasError &&
              snapshot.connectionState == ConnectionState.active) {
            if ((snapshot.data as List<OrderEntity>).isNotEmpty) {
              var orderedList = (snapshot.data as List<OrderEntity>);
              return SizedBox(
                  height: MediaQuery.of(context).size.height * 0.60,
                  child: GridView.builder(
                      gridDelegate:
                          const SliverGridDelegateWithFixedCrossAxisCount(
                              childAspectRatio: 3,
                              crossAxisCount: 1,
                              crossAxisSpacing: 10,
                              mainAxisSpacing: 5),
                      itemCount: orderedList.length,
                      itemBuilder: (context, position) {
                        print(
                            'orderedList[position] : ${orderedList[position].orderID}');
                        return GestureDetector(
                          onTap: () =>
                              onOrderCardSelectedHandler(orderedList[position]),
                          child: Card(
                            child: Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Text('Order ${position + 1}'),
                                      Text(
                                          'Items Ordered : ${orderedList[position].items.length}'),
                                      Text(
                                          'Ordered Amount : ${orderedList[position].orderedTotalPrice}'),
                                    ],
                                  ),
                                ]),
                          ),
                        );
                      }));
            }
            return const Text('Orders list is empty!');
          }
          return const Text('You can view orders list here');
        });
  }
}
