import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:my_mart/otherFiles/Screens/ShowLoadingScreen.dart';

import '../../main.dart';
import '../../objectbox.g.dart';
import '../../services/FireStoreServices/ProductServices.dart';
import '../../utility/ObjectBoxEntities/ProductCat/EntityForProducts.dart';
import '../ProductDetails.dart';

class TabBarViewWishList extends StatefulWidget {
  const TabBarViewWishList({Key? key}) : super(key: key);

  @override
  State<TabBarViewWishList> createState() => _TabBarViewWishListState();
}

class _TabBarViewWishListState extends State<TabBarViewWishList> {
  late Stream<List<ProductsListEntity>> _productsInWishListStream;

  @override
  void initState() {
    setState(() {
      QueryBuilder<ProductsListEntity> builder = objectboxStore
          .box<ProductsListEntity>()
          .query(ProductsListEntity_.productDetails.notNull());
      builder.backlink(ProductDetailsEntity_.product,
          ProductDetailsEntity_.isInWish.equals(true));
      _productsInWishListStream =
          builder.watch(triggerImmediately: true).map((e) => e.find());
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
        stream: _productsInWishListStream,
        builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
          if (snapshot.hasData &&
              !snapshot.hasError &&
              snapshot.connectionState == ConnectionState.active) {
            var wishList = snapshot.data as List<ProductsListEntity>;
            if (wishList.isNotEmpty) {
              return Column(
                children: [
                  Expanded(
                    child: GridView.builder(
                        gridDelegate:
                            const SliverGridDelegateWithFixedCrossAxisCount(
                                childAspectRatio: 3,
                                crossAxisCount: 1,
                                crossAxisSpacing: 10,
                                mainAxisSpacing: 5),
                        itemCount: wishList.length,
                        itemBuilder: (context, position) {
                          return GestureDetector(
                            onTap: () => {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (BuildContext context) =>
                                          ProductDetailsScreen(
                                              catId: wishList[position]
                                                  .productCat
                                                  .target!
                                                  .catID
                                                  .toString(),
                                              productId: wishList[position]
                                                  .productID
                                                  .toString(),
                                              productInfo: {
                                                'catName': wishList[position]
                                                    .productCat
                                                    .target!
                                                    .catName,
                                                'productName':
                                                    wishList[position]
                                                        .productName!,
                                                'productImg': wishList[position]
                                                    .productImg!
                                              })))
                            },
                            child: Card(
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  FittedBox(
                                    child: SizedBox(
                                      width: MediaQuery.of(context).size.width *
                                          0.45,
                                      child: CachedNetworkImage(
                                        placeholder: (context, url) =>
                                            const Center(
                                                child: ShowLoadingScreen(
                                                    color: Colors.white)),
                                        imageUrl:
                                            wishList[position].productImg!,
                                      ),
                                    ),
                                  ),
                                  FittedBox(
                                    child: SizedBox(
                                        width:
                                            MediaQuery.of(context).size.width *
                                                0.52,
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceEvenly,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.spaceEvenly,
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.center,
                                              children: [
                                                SizedBox(
                                                  width: MediaQuery.of(context)
                                                          .size
                                                          .width *
                                                      0.20,
                                                  child: Text(wishList[position]
                                                      .productDetails
                                                      .target!
                                                      .productCompanyName!),
                                                ),
                                                SizedBox(
                                                  width: MediaQuery.of(context)
                                                          .size
                                                          .width *
                                                      0.32,
                                                  child: Text(wishList[position]
                                                      .productName!),
                                                ),
                                              ],
                                            ),
                                            Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.spaceEvenly,
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.center,
                                              children: [
                                                SizedBox(
                                                  width: MediaQuery.of(context)
                                                          .size
                                                          .width *
                                                      0.20,
                                                  child: Text(
                                                      '${wishList[position].productDetails.target!.productWeight}'),
                                                ),
                                                SizedBox(
                                                  width: MediaQuery.of(context)
                                                          .size
                                                          .width *
                                                      0.32,
                                                  child: Text(
                                                      '${wishList[position].productDetails.target!.productPrice} Rs.'),
                                                ),
                                              ],
                                            ),
                                            Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.center,
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.center,
                                              children: [
                                                OutlinedButton(
                                                  onPressed: () => ProductServices()
                                                      .addOrRemoveProductToWishList(
                                                          wishList[position]
                                                              .productDetails
                                                              .target!
                                                              .isInWish!,
                                                          wishList[position]
                                                              .productCat
                                                              .target!
                                                              .catID
                                                              .toString(),
                                                          wishList[position]
                                                              .productID
                                                              .toString(),
                                                          objectboxStore.box<
                                                              ProductsListEntity>()),
                                                  child: const Text('Remove'),
                                                )
                                              ],
                                            )
                                          ],
                                        )),
                                  ),
                                ],
                              ),
                            ),
                          );
                        }),
                  ),
                ],
              );
            }
            return const Text('Wish List is empty, add some products!');
          }
          return const ShowLoadingScreen(color: Colors.white);
        });
  }
}
