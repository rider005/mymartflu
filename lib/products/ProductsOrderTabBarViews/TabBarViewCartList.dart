import 'dart:convert';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import 'package:my_mart/objectbox.g.dart';
import 'package:my_mart/otherFiles/Screens/ShowLoadingScreen.dart';
import 'package:my_mart/services/FireStoreServices/ProductServices.dart';
import 'package:my_mart/utility/Utilities.dart';
import 'package:razorpay_flutter/razorpay_flutter.dart';

import '../../main.dart';
import '../../services/AuthServices.dart';
import '../../services/FireStoreServices/FirestoreServices.dart';
import '../../utility/ObjectBoxEntities/AppData/EntityAppData.dart';
import '../../utility/ObjectBoxEntities/ProductCat/EntityForProducts.dart';
import '../../utility/ObjectBoxEntities/User/UserEntity.dart';
import '../ProductDetails.dart';

class TabBarViewCartList extends StatefulWidget {
  const TabBarViewCartList({Key? key}) : super(key: key);

  @override
  State<TabBarViewCartList> createState() => _TabBarViewCartListState();
}

class _TabBarViewCartListState extends State<TabBarViewCartList>
    with SingleTickerProviderStateMixin {
  late Stream<List<ProductDetailsEntity>> _productsInCartListStream;
  late UserEntity userDetails;
  bool isShowingAddress = false,
      showAnim = true,
      isOrdering = false,
      isOrdered = false,
      isTest = true;
  late AnimationController _controller;
  int addressLen = 20;
  late String animationJsonLink =
      'https://assets5.lottiefiles.com/packages/lf20_ydsNOn.json',
      bottomBtnTxt = 'Next',
      razorPayOrderID = '';
  late Razorpay _razorpay;
  late DocumentReference storeOrderDoc;
  late List ordersList = [];
  late Iterable<ProductDetailsEntity> cartList = [];
  var totalProductsPrice = 0.0;
  Box<ProductDetailsEntity> box = objectboxStore.box<ProductDetailsEntity>();
  Box<ProductsListEntity> boxProductLst =
  objectboxStore.box<ProductsListEntity>();

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
        stream: _productsInCartListStream,
        builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
          Widget currentWidget = const Text('Wait');
          if (showAnim) {
            currentWidget = Center(
                child: Lottie.network(animationJsonLink,
                    controller: _controller,
                    repeat: true,
                    fit: BoxFit.cover, onLoaded: (composition) {
                      _controller
                        ..duration = composition.duration
                        ..forward().whenComplete(() =>
                            setState(() =>
                            {if (!isOrdering) showAnim = false else
                              showAnim = true}));
                    }));
          } else if (snapshot.connectionState == ConnectionState.waiting ||
              snapshot.hasError ||
              !snapshot.hasData) {
            currentWidget = const ShowLoadingScreen(color: Colors.white);
          } else if (snapshot.connectionState == ConnectionState.active) {
            var cartList = snapshot.data as List<ProductDetailsEntity>;
            if (cartList.isEmpty) {
              currentWidget =
              const Text('List is empty, add some items to cart list!');
            } else {
              totalProductsPrice = 0.0;
              for (var cart in cartList) {
                totalProductsPrice = totalProductsPrice +
                    (double.tryParse(cart.productPrice.toString())! *
                        cart.qtyInCart!);
              }
              // var heightForGrid = !isShowingAddress ? 0.60 : 0.47;
              currentWidget = Column(
                children: [
                  if (isShowingAddress)
                    Column(
                      children: [
                        Row(
                          children: [
                            const Text('Will deliver to : '),
                            Text('${userDetails.userName}'),
                          ],
                        ),
                        userDetails.userAddress!.length > addressLen
                            ? Row(
                          children: [
                            const Text('Address : '),
                            Flexible(
                              child: Text(
                                '${userDetails.userAddress}',
                              ),
                            ),
                          ],
                        )
                            : const Text(
                            'Go to profile and update your address'),
                      ]
                          .map((e) =>
                          Padding(
                            padding: const EdgeInsets.symmetric(
                                vertical: 4, horizontal: 4),
                            child: e,
                          ))
                          .toList(),
                    ),
                  Expanded(
                      child: GridView.builder(
                          gridDelegate:
                          const SliverGridDelegateWithFixedCrossAxisCount(
                              childAspectRatio: 3,
                              crossAxisCount: 1,
                              crossAxisSpacing: 10,
                              mainAxisSpacing: 5),
                          itemCount: cartList.length,
                          itemBuilder: (context, position) {
                            Stream<List<ProductsListEntity>>
                            productsNameImgInCartListStream;
                            productsNameImgInCartListStream = objectboxStore
                                .box<ProductsListEntity>()
                            // The simplest possible query that just gets ALL the data out of the Box
                                .query(ProductsListEntity_.productID.equals(
                                cartList[position]
                                    .product
                                    .target!
                                    .productID!))
                                .watch(triggerImmediately: true)
                                .map((event) => event.find());
                            return StreamBuilder(
                                stream: productsNameImgInCartListStream,
                                builder: (BuildContext context,
                                    AsyncSnapshot<dynamic> snapshot) {
                                  debugPrint('snapshot2 : $snapshot');
                                  if (snapshot.hasData &&
                                      !snapshot.hasError &&
                                      snapshot.connectionState ==
                                          ConnectionState.active) {
                                    var productNameImg = snapshot.data
                                    as List<ProductsListEntity>;
                                    if (productNameImg.isNotEmpty) {
                                      ProductServices()
                                          .listenProductDetailsChanges(
                                          productNameImg.first.productCat
                                              .target!.catID
                                              .toString(),
                                          cartList[position]
                                              .product
                                              .target!
                                              .productID!);
                                      Iterable<ProductCategoriesEntity> data;
                                      return GestureDetector(
                                        onTap: () =>
                                        {
                                          data = objectboxStore
                                              .box<ProductCategoriesEntity>()
                                              .getAll()
                                              .where((element) =>
                                          element.catID ==
                                              productNameImg
                                                  .first
                                                  .productCat
                                                  .target
                                                  ?.catID!),
                                          Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (BuildContext
                                                  context) =>
                                                      ProductDetailsScreen(
                                                          catId: productNameImg
                                                              .first
                                                              .productCat
                                                              .target!
                                                              .catID!,
                                                          productId:
                                                          productNameImg
                                                              .first
                                                              .productID!,
                                                          productInfo: {
                                                            'catName': data
                                                                .first.catName,
                                                            'productName':
                                                            productNameImg
                                                                .first
                                                                .productName!,
                                                            'productImg':
                                                            productNameImg
                                                                .first
                                                                .productImg!
                                                          })))
                                        },
                                        child: Card(
                                          child: Row(
                                            crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                            mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                            children: [
                                              FittedBox(
                                                child: SizedBox(
                                                  width: MediaQuery
                                                      .of(context)
                                                      .size
                                                      .width *
                                                      0.45,
                                                  child: CachedNetworkImage(
                                                    placeholder: (context,
                                                        url) =>
                                                    const Center(
                                                        child:
                                                        ShowLoadingScreen(
                                                            color: Colors
                                                                .white)),
                                                    imageUrl: productNameImg
                                                        .first.productImg!,
                                                  ),
                                                ),
                                              ),
                                              FittedBox(
                                                child: SizedBox(
                                                    width:
                                                    MediaQuery
                                                        .of(context)
                                                        .size
                                                        .width *
                                                        0.52,
                                                    child: Column(
                                                      mainAxisAlignment:
                                                      MainAxisAlignment
                                                          .spaceEvenly,
                                                      crossAxisAlignment:
                                                      CrossAxisAlignment
                                                          .start,
                                                      children: [
                                                        Row(
                                                          mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .spaceBetween,
                                                          crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .center,
                                                          children: [
                                                            Text(
                                                              productNameImg
                                                                  .first
                                                                  .productName!
                                                                  .length >
                                                                  17
                                                                  ? '${productNameImg
                                                                  .first
                                                                  .productName!
                                                                  .substring(0,
                                                                  16)} ...'
                                                                  : productNameImg
                                                                  .first
                                                                  .productName!,
                                                            ),
                                                            SizedBox(
                                                              height: 30,
                                                              width: 40,
                                                              child:
                                                              OutlinedButton(
                                                                  style:
                                                                  ButtonStyle(
                                                                    backgroundColor:
                                                                    MaterialStateProperty
                                                                        .all(
                                                                        Colors
                                                                            .green),
                                                                  ),
                                                                  onPressed:
                                                                      () =>
                                                                  {
                                                                    if (cartList[position]
                                                                        .qtyInCart! >
                                                                        1)
                                                                      {
                                                                        _decreaseProductQtyByID(
                                                                            cartList[position]
                                                                                .product
                                                                                .target!
                                                                                .productID!)
                                                                      }
                                                                    else
                                                                      {
                                                                        Utilities()
                                                                            .showSnackBar(
                                                                            "Click remove button to remove")
                                                                      }
                                                                  },
                                                                  child:
                                                                  const Text(
                                                                    '-',
                                                                    style: TextStyle(
                                                                        color:
                                                                        Colors
                                                                            .white),
                                                                  )),
                                                            )
                                                          ],
                                                        ),
                                                        Row(
                                                          mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .spaceBetween,
                                                          crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .center,
                                                          children: [
                                                            Text(
                                                                '${cartList[position]
                                                                    .productWeight}'),
                                                            ClipRRect(
                                                                borderRadius:
                                                                const BorderRadius
                                                                    .all(
                                                                    Radius
                                                                        .circular(
                                                                        10)),
                                                                child: Container(
                                                                    margin: const EdgeInsets
                                                                        .only(
                                                                        right:
                                                                        15),
                                                                    child: Text(
                                                                        "${cartList[position]
                                                                            .qtyInCart}")))
                                                          ],
                                                        ),
                                                        Row(
                                                          mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .spaceBetween,
                                                          crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .center,
                                                          children: [
                                                            Text(
                                                                '${cartList[position]
                                                                    .productPrice} Rs.'),
                                                            SizedBox(
                                                              height: 30,
                                                              width: 40,
                                                              child:
                                                              OutlinedButton(
                                                                  style:
                                                                  ButtonStyle(
                                                                    backgroundColor:
                                                                    MaterialStateProperty
                                                                        .all(
                                                                        Colors
                                                                            .green),
                                                                  ),
                                                                  onPressed: () =>
                                                                      _increaseProductQtyByID(
                                                                          cartList[
                                                                          position]
                                                                              .product
                                                                              .target!
                                                                              .productID!),
                                                                  child:
                                                                  const Text(
                                                                    '+',
                                                                    style: TextStyle(
                                                                        color:
                                                                        Colors
                                                                            .white),
                                                                  )),
                                                            ),
                                                          ],
                                                        ),
                                                        Row(
                                                          mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .end,
                                                          crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .center,
                                                          children: [
                                                            OutlinedButton(
                                                              style:
                                                              ButtonStyle(
                                                                backgroundColor:
                                                                MaterialStateProperty
                                                                    .all(Colors
                                                                    .green),
                                                              ),
                                                              onPressed: () =>
                                                                  ProductServices()
                                                                      .removeProductFromCart(
                                                                      '${productNameImg
                                                                          .first
                                                                          .productCat
                                                                          .target
                                                                          ?.catID}',
                                                                      '${cartList[position]
                                                                          .product
                                                                          .target!
                                                                          .productID}',
                                                                      cartList[
                                                                      position]
                                                                          .qtyInCart!
                                                                          .toInt()),
                                                              child: const Text(
                                                                'Remove',
                                                                style: TextStyle(
                                                                    color: Colors
                                                                        .white),
                                                              ),
                                                            )
                                                          ],
                                                        )
                                                      ],
                                                    )),
                                              ),
                                            ],
                                          ),
                                        ),
                                      );
                                    }
                                  }
                                  return Container();
                                });
                          })),
                  SizedBox(
                    height: MediaQuery
                        .of(context)
                        .size
                        .height * 0.13,
                    child: Padding(
                      padding: const EdgeInsets.only(right: 10.0, left: 10.0),
                      child: Card(
                        color: Colors.green,
                        shape: const RoundedRectangleBorder(
                            borderRadius:
                            BorderRadius.all(Radius.circular(30))),
                        child: Padding(
                          padding:
                          const EdgeInsets.only(right: 10.0, left: 10.0),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                '$totalProductsPrice Rs.',
                                style: const TextStyle(
                                  color: Colors.white,
                                ),
                              ),
                              OutlinedButton(
                                  onPressed: () => _orderCartListProducts(),
                                  child: Text(
                                    bottomBtnTxt,
                                    style: const TextStyle(color: Colors.white),
                                  )),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              );
            }
          }
          return AnimatedSwitcher(
            duration: const Duration(seconds: 1),
            child: currentWidget,
          );
        });
  }

  _orderCartListProducts() async {
    if (!isShowingAddress) {
      setState(() {
        isShowingAddress = true;
        isOrdering = false;
        isOrdered = false;
      });
      setState(() => bottomBtnTxt = userDetails.userAddress!.length < 20
          ? 'GO TO PROFILE'
          : 'Place Order');
      return;
    } else if (userDetails.userAddress!.length < 20) {
      bottomNavSelectedIndex.value = 3;
      Utilities().showSnackBar('Update Your address before placing the order!');
      return;
    }
    setState(() {
      animationJsonLink =
          'https://assets6.lottiefiles.com/packages/lf20_M3Uk5P.json';
      showAnim = true;
      isOrdering = true;
      isOrdered = false;
      isShowingAddress = false;
      bottomBtnTxt = 'Place Order';
    });
    cartList = [];
    cartList = box.getAll().where((element) => element.isInCart == true);
    // List<ItemEntity> orderedItemsList = [];
    ordersList = [];
    OrderEntity orderEntity = OrderEntity();
    for (var cart in cartList) {
      ordersList.add({
        'catID': cart.product.target!.productCat.target!.catID,
        'productID': cart.product.target!.productID,
        'qtyForOrder': cart.qtyInCart,
        'productPrice': cart.productPrice
      });
      var item = OrderItemEntity();
      item.catID = cart.product.target!.productCat.target!.catID;
      item.productID = cart.product.target!.productID;
      item.qtyForOrder = cart.qtyInCart;
      item.productPrice = cart.productPrice;
      orderEntity.items.add(item);
    }
    try {
      storeOrderDoc = FirestoreServices()
          .allUsersCollRef
          .doc(AuthServices().currentUser!.uid)
          .collection(FirestoreServices.ordersCollName)
          .doc();
      totalProductsPrice = totalProductsPrice * 100;
      var reqBody = jsonEncode(<String, dynamic>{
        'amount': totalProductsPrice,
        'currency': 'INR',
        'receipt': storeOrderDoc.id,
        "transfers": [
          {
            "account": "acc_L5isOqszsIQ8bk",
            "amount": totalProductsPrice * 0.95,
            "currency": "INR",
          }
        ]
      });
      var orderCreateRequest = await Utilities().razorPayRequestByURLAndBody(
          isTest, Utilities().getRazorPayOrderUrl(), reqBody);
      var body = jsonDecode(orderCreateRequest.body);
      if (body['id'] != null) {
        razorPayOrderID = body['id'];
        razorPayCheckout(Utilities().username, body['id'], totalProductsPrice);
      } else {
        debugPrint('orderCreateRequest Failed: ${orderCreateRequest.body}');
      }
    } catch (e) {
      setState(() {
        showAnim = false;
        isOrdering = false;
        isOrdered = false;
      });
      debugPrint('Catch : Method : _orderCartListProducts : Create Order');
      debugPrint('E :$e');
    }
  }

  razorPayCheckout(String razorKey, String orderID, totalPrice) {
    var appData = objectboxStore
        .box<EntityAppData>()
        .getAll()
        .first;
    var options = {
      'key': razorKey,
      'amount': totalPrice, //in the smallest currency sub-unit.
      'order_id': orderID.trim(), // Generate order_id using Orders API
      "currency": "INR",
      'name': 'E-Shop',
      'description': 'Your order is about to place!',
      'image':
      'https://1000logos.net/wp-content/uploads/2021/03/Paytm_Logo.png',
      'prefill': {
        'contact':
        userDetails.userPhone ?? 'You can update phone number later!',
        'email': userDetails.userEmail
      },
      'timeout': 600, // in seconds
      'remember_customer': true,
      'send_sms_hash': true,
      'config': {
        'display': {
          'language': appData.userCurrentLanguage == 'gu'
              ? 'guj'
              : appData.userCurrentLanguage == 'hi'
                  ? 'hi'
                  : 'en'
        }
      },
    };
    _razorpay.open(options);
  }

  @override
  void initState() {
    setState(() {
      _razorpay = Razorpay();
      _productsInCartListStream = objectboxStore
          .box<ProductDetailsEntity>()
          .query(ProductDetailsEntity_.isInCart.equals(true))
          .watch(triggerImmediately: true)
          .map((query) => query.find());
      if (objectboxStore
              .box<UserEntity>()
              .query(
                  UserEntity_.userUID.equals(AuthServices().currentUser!.uid))
              .build()
              .count() >
          0) {
        userDetails = objectboxStore.box<UserEntity>().getAll().first;
      }
    });
    _controller =
        AnimationController(vsync: this, duration: const Duration(seconds: 3));
    _controller.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        if (isOrdering) {
          _controller.duration = const Duration(seconds: 3);
          _controller.repeat(reverse: true);
        } else if (isOrdered) {
          _controller.forward().whenComplete(() {
            setState(() {
              isOrdered = false;
              showAnim = false;
            });
            _controller.reset();
          });
        } else {
          setState(() {
            showAnim = false;
          });
          _controller.reset();
        }
      }
    });
    _razorpay.on(Razorpay.EVENT_PAYMENT_SUCCESS, _handlePaymentSuccess);
    _razorpay.on(Razorpay.EVENT_PAYMENT_ERROR, _handlePaymentError);
    _razorpay.on(Razorpay.EVENT_EXTERNAL_WALLET, _handleExternalWallet);
    super.initState();
  }

  Future<void> _handlePaymentSuccess(PaymentSuccessResponse response) async {
    // Do something when payment succeeds
    debugPrint('orderCreateRequest Success O:  ${response.orderId}');
    debugPrint('orderCreateRequest Success P:  ${response.paymentId}');
    debugPrint('orderCreateRequest Success S:  ${response.signature}');

    await storeOrderDoc.set({
      'orderList': ordersList,
      'orderedTime': FieldValue.serverTimestamp(),
      'orderedToAddress': userDetails.userAddress,
      'orderedTotalPrice': totalProductsPrice / 100,
      'razorpayOrderID': razorPayOrderID,
      'razorpayPaymentId': response.paymentId,
      'razorpaySignature': response.signature,
      'sellerPerson': 'GJwtOjjX33UB310vf3B8O8Els122'
    });

    Future.delayed(
        const Duration(seconds: 1),
        () => {
              setState(() {
                animationJsonLink =
                    'https://assets1.lottiefiles.com/packages/lf20_d2dehrdw.json';
                showAnim = true;
                isOrdering = false;
                isOrdered = true;
              }),
              Utilities().showSnackBar('Order is placed success!')
            });
  }

  void _handlePaymentError(PaymentFailureResponse response) {
    // Do something when payment fails
    debugPrint('orderCreateRequest Error :  ${response.message}');
    setState(() {
      animationJsonLink =
          'https://assets8.lottiefiles.com/private_files/lf30_jq4i7W.json';
      showAnim = true;
      isOrdering = false;
      isOrdered = true;
    });
    Utilities().showSnackBar(response.message ?? 'Try again later!');
  }

  void _handleExternalWallet(ExternalWalletResponse response) {
    // Do something when an external wallet is selected
    debugPrint('orderCreateRequest ExternalWallet :  $response');
    setState(() {
      animationJsonLink =
          'https://assets1.lottiefiles.com/packages/lf20_d2dehrdw.json';
      showAnim = true;
      isOrdering = false;
      isOrdered = true;
    });
    // Utilities().showSnackBar('Order is placed success!');
  }

  _increaseProductQtyByID(String productID) async {
    var fireStoreBatch = fireStore.batch();
    fireStoreBatch.update(
        FirestoreServices()
            .allUsersCollRef
            .doc(AuthServices().currentUser!.uid)
            .collection('userCartList')
            .doc(productID),
        {'qtyInCart': FieldValue.increment(1)});
    fireStoreBatch.update(
        FirestoreServices()
            .allUsersCollRef
            .doc(AuthServices().currentUser!.uid),
        {'userCart': FieldValue.increment(1)});

    await fireStoreBatch.commit();
  }

  _decreaseProductQtyByID(String productID) async {
    var fireStoreBatch = fireStore.batch();
    fireStoreBatch.update(
        FirestoreServices()
            .allUsersCollRef
            .doc(AuthServices().currentUser!.uid)
            .collection('userCartList')
            .doc(productID),
        {'qtyInCart': FieldValue.increment(-1)});
    fireStoreBatch.update(
        FirestoreServices()
            .allUsersCollRef
            .doc(AuthServices().currentUser!.uid),
        {'userCart': FieldValue.increment(-1)});

    await fireStoreBatch.commit();
  }

  @override
  dispose() {
    _controller.dispose();
    _razorpay.clear(); // Removes all listeners
    super.dispose();
  }
}
