import 'package:animations/animations.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:my_mart/objectbox.g.dart';
import 'package:my_mart/otherFiles/Screens/ShowLoadingScreen.dart';

import '../main.dart';
import '../utility/ObjectBoxEntities/ProductCat/EntityForProducts.dart';
import 'ProductDetails.dart';

class OrderDetailsScreen extends StatefulWidget {
  final OrderEntity mySelectedOrderDetails;

  const OrderDetailsScreen({Key? key, required this.mySelectedOrderDetails})
      : super(key: key);

  @override
  State<OrderDetailsScreen> createState() => _OrderDetailsScreenState();
}

class _OrderDetailsScreenState extends State<OrderDetailsScreen> {
  late Box<ProductDetailsEntity> productDetailsBox =
      objectboxStore.box<ProductDetailsEntity>();

  @override
  Widget build(BuildContext context) {
    ToMany<OrderItemEntity> orderedItems = widget.mySelectedOrderDetails.items;
    return Scaffold(
      appBar: AppBar(
        title: const Text('Order Details'),
      ),
      body: Center(
        child: Column(
          children: [
            SizedBox(
                height: MediaQuery
                    .of(context)
                    .size
                    .height * 0.20,
                width: MediaQuery
                    .of(context)
                    .size
                    .width * 0.90,
                child: Card(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      // Text('orderedTime : ${widget.myOrdersDetails['orderedTime']}'),
                      Text(
                          'Total Price : ${widget.mySelectedOrderDetails.orderedTotalPrice}'),
                      Text(
                          'Delivery Address : ${widget.mySelectedOrderDetails.orderedToAddress}'),
                    ],
                  ),
                )),
            Expanded(
              child: GridView.builder(
                  gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                      childAspectRatio: 3,
                      crossAxisCount: 1,
                      crossAxisSpacing: 10,
                      mainAxisSpacing: 5),
                  itemCount: orderedItems.length,
                  itemBuilder: (context, position) {
                    Stream<List<ProductsListEntity>>
                        productsNameImgInOrderedListStream;
                    productsNameImgInOrderedListStream = objectboxStore
                        .box<ProductsListEntity>()
                        .query(ProductsListEntity_.productID.equals(
                        orderedItems[position].productID!))
                        .watch(triggerImmediately: true)
                        .map((event) => event.find());
                    return StreamBuilder(
                      stream: productsNameImgInOrderedListStream,
                      builder: (BuildContext context,
                          AsyncSnapshot<dynamic> snapshot) {
                        if (snapshot.hasData &&
                            !snapshot.hasError &&
                            snapshot.connectionState ==
                                ConnectionState.active) {
                          var productNameImg =
                              (snapshot.data as List<ProductsListEntity>).first;
                          ProductDetailsEntity productDetails =
                              productDetailsBox
                                  .getAll()
                                  .where((element) =>
                              element.product.target!.productID ==
                                  orderedItems[position].productID)
                                  .first;
                          return OpenContainer(
                              transitionDuration:
                                  const Duration(milliseconds: 800),
                              transitionType: ContainerTransitionType.fade,
                              closedBuilder: (context, action) {
                                return GestureDetector(
                                  onTap: action,
                                  child: Card(
                                    child: Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        FittedBox(
                                          child: SizedBox(
                                            width: MediaQuery.of(context)
                                                    .size
                                                    .width *
                                                0.45,
                                            child: CachedNetworkImage(
                                              placeholder: (context, url) =>
                                                  const Center(
                                                      child: ShowLoadingScreen(
                                                          color: Colors.white)),
                                              imageUrl: productNameImg
                                                  .productImg
                                                  .toString(),
                                            ),
                                          ),
                                        ),
                                        FittedBox(
                                          child: SizedBox(
                                              width: MediaQuery.of(context)
                                                      .size
                                                      .width *
                                                  0.52,
                                              child: Column(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceEvenly,
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .spaceEvenly,
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .center,
                                                    children: [
                                                      SizedBox(
                                                        width: MediaQuery.of(
                                                                    context)
                                                                .size
                                                                .width *
                                                            0.20,
                                                        child: Text(productDetails
                                                            .productCompanyName
                                                            .toString()),
                                                      ),
                                                      SizedBox(
                                                        width: MediaQuery.of(
                                                                    context)
                                                                .size
                                                                .width *
                                                            0.32,
                                                        child: Text(
                                                            productNameImg
                                                                .productName!),
                                                      ),
                                                    ],
                                                  ),
                                                  Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .spaceEvenly,
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .center,
                                                    children: [
                                                      SizedBox(
                                                        width: MediaQuery.of(
                                                                    context)
                                                                .size
                                                                .width *
                                                            0.20,
                                                        child: Text(
                                                            '${productDetails.productWeight}'),
                                                      ),
                                                      SizedBox(
                                                        width: MediaQuery.of(
                                                                    context)
                                                                .size
                                                                .width *
                                                            0.32,
                                                        child: Text(
                                                            '${productDetails.productPrice} Rs.'),
                                                      ),
                                                    ],
                                                  ),
                                                  Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .spaceEvenly,
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .center,
                                                    children: [
                                                      SizedBox(
                                                        width: MediaQuery.of(
                                                                    context)
                                                                .size
                                                                .width *
                                                            0.20,
                                                        child: Container(),
                                                      ),
                                                      SizedBox(
                                                        width: MediaQuery.of(
                                                                    context)
                                                                .size
                                                                .width *
                                                            0.32,
                                                        child: Text(
                                                            'Ordered QTY : ${orderedItems[position].qtyForOrder}'),
                                                      ),
                                                    ],
                                                  ),
                                                ],
                                              )),
                                        ),
                                      ],
                                    ),
                                  ),
                                );
                              },
                              openBuilder: (context, action) {
                                return ProductDetailsScreen(
                                    catId: productNameImg
                                        .productCat.target!.catID
                                        .toString(),
                                    productId: productNameImg.productID!,
                                    productInfo: {
                                      'catName': productNameImg
                                          .productCat.target!.catName,
                                      'productName': productNameImg.productName,
                                      'productImg': productNameImg.productImg
                                    });
                              });
                        }
                        return const ShowLoadingScreen(color: Colors.white);
                      },
                    );
                  }),
            ),
          ],
        ),
      ),
    );
  }
}
