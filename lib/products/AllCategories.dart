import 'package:animations/animations.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:my_mart/main.dart';
import 'package:my_mart/otherFiles/Screens/ShowLoadingScreen.dart';
import 'package:my_mart/products/ProductsList.dart';
import 'package:translator/translator.dart';

import '../utility/ObjectBoxEntities/ProductCat/EntityForProducts.dart';
import '../utility/Utilities.dart';

class AllCategoriesScreen extends StatefulWidget {
  const AllCategoriesScreen({Key? key}) : super(key: key);

  @override
  AllCategoriesScreenState createState() => AllCategoriesScreenState();
}

class AllCategoriesScreenState extends State<AllCategoriesScreen> {
  late Stream<List<ProductCategoriesEntity>> _stream;

  @override
  void initState() {
    setState(() {
      // 👇 ADD THIS
      _stream = objectboxStore
          .box<ProductCategoriesEntity>()
          // The simplest possible query that just gets ALL the data out of the Box
          .query()
          .watch(triggerImmediately: true)
          // Watching the query produces a Stream<Query<ProductCategories>>
          // To get the actual data inside a List<ProductCategories>, we need to call find() on the query
          .map((query) => query.find());
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Utilities().startScreenTracking('All Categories');

    return StreamBuilder(
        stream: _stream,
        builder: (ctx, snapshot) {
          if (snapshot.hasError ||
              snapshot.connectionState == ConnectionState.waiting ||
              !snapshot.hasData) {
            return const ShowLoadingScreen(color: Colors.white);
          } else if (snapshot.connectionState == ConnectionState.active) {
            var productCatList = snapshot.data as List<ProductCategoriesEntity>;
            List<Future> productFutures = [];
            for (var product in productCatList) {
              productFutures.add(GoogleTranslator().translate(
                  product.catName.toString(),
                  to: Get.locale.toString().substring(0, 2)));
            }
            if (productFutures.isNotEmpty) {
              return FutureBuilder(
                future: Future.wait(productFutures),
                builder: (ctx, snapshot) {
                  if (snapshot.hasData && !snapshot.hasError) {
                    var productCatNames = snapshot.data as List;
                    return GridView.builder(
                        gridDelegate:
                            const SliverGridDelegateWithFixedCrossAxisCount(
                                childAspectRatio: 1,
                                crossAxisCount: 2,
                                crossAxisSpacing: 10,
                                mainAxisSpacing: 10),
                        itemCount: productCatNames.length,
                        itemBuilder: (context, position) {
                          return Utilities().customContainer(OpenContainer(
                            closedColor: Colors.transparent,
                            middleColor: Colors.transparent,
                            openColor: Colors.transparent,
                            transitionDuration:
                                const Duration(milliseconds: 800),
                            transitionType: ContainerTransitionType.fade,
                            closedBuilder: (context, action) {
                              return GestureDetector(
                                  onTap: action,
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      Utilities().clipImg(CachedNetworkImage(
                                        height:
                                            MediaQuery.of(context).size.height /
                                                6,
                                        width:
                                            MediaQuery.of(context).size.width /
                                                2.5,
                                        imageUrl: productCatList[position]
                                            .catImg
                                            .toString(),
                                        fit: BoxFit.fill,
                                        placeholder: (context, url) =>
                                            const Center(
                                                child: ShowLoadingScreen(
                                                    color: Colors.white)),
                                      )),
                                      Text(productCatNames[position].toString(),
                                          style: const TextStyle(
                                              color: Colors.white),
                                          overflow: TextOverflow.ellipsis,
                                          softWrap: false,
                                          maxLines: 2,
                                          textScaleFactor: 1.5,
                                          textAlign: TextAlign.center),
                                    ],
                                  ));
                            },
                            openBuilder: (context, action) {
                              return ProductsListScreen(
                                  catID:
                                      productCatList[position].catID.toString(),
                                  catName: productCatList[position]
                                      .catName
                                      .toString());
                            },
                          ));
                        });
                  }
                  return const ShowLoadingScreen(color: Colors.white);
                },
              );
            }
            return const ShowLoadingScreen(color: Colors.white);
          }
          return const ShowLoadingScreen(color: Colors.white);
        });
  }
}
